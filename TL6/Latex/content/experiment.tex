\chapter{Experiment}
\label{chap:experiment}
Um die Forschungsfrage F3 beantworten zu können, wird in diesem Kapitel das Experiment vorgestellt. 

\section{Aufbau}
\label{sec:aufbau}
Für das Experiment wurde eine Virtuelle Maschine aufgesetzt.
Die dafür konfigurierten Daten sind aus der \autoref{tab:vm} zu entnehmen.

\begin{table}[H]
    \setlength{\arrayrulewidth}{0.5mm}
    \setlength{\tabcolsep}{18pt}
    \renewcommand{\arraystretch}{1.5}
    \centering
    \begin{tabular}{p{2cm}|p{4cm}|p{2cm}|p{2cm}}
    \hline
        OS & CPU  & RAM & SSD  \\ \hline
        Ubuntu 22.04 & 3 x Intel Core i7-4790 (4 x 3,6 GHz)  & 4096 MB & 30 GB \\ \hline 
    \end{tabular}
    \caption{VM Konfiguration}
    \label{tab:vm}
\end{table}

Im Experiment wird eine API in ein lokales Kubernetes Cluster publiziert und mittels eines Ingress Controllers geroutet. 
Für die Erstellung des lokalen Kubernetes Clusters wird das Tool \textit{minikube} verwendet \autocite{minikube_minikube_nodate}.
Als API wird das Podinfo Image als Grundlage verwendet.
Podinfo ist eine schlanke Webanwendung, welche in Go erstellt wurde und Best Practices für die Ausführung von Microservices in Kubernetes zeigt \autocite{prodan_podinfo_2022}.
Für die Implementierung eines Ingress Controllers, wird im Experiment der NGINX Controller verwendet, welcher neben Sicherheit und API Management auch eine hohe Loadbalancer Performance bietet \autocite{nginx_nginx_nodate}.
Für eine schnelle und einfache Installation von z.B. der nginx Bibliothek nach Kubernetes, wird das Tool Helm verwendet \autocite{noauthor_helm_nodate}. 

Anschließend wird die Performance der API getestet.
Hierfür wird ein Locust Dienst nach Kubernetes bereitgestellt, welches mittels eines Pythonscript, die Performance von Webanwendungen unter hoher Auslastung testen kann \autocite{locust_what_nodate}. 
\autoref{tab:tools} zeigt eine Übersicht, der im Experiment verwendeten Tools.

\begin{table}[H]
    \setlength{\arrayrulewidth}{0.5mm}
    \setlength{\tabcolsep}{18pt}
    \renewcommand{\arraystretch}{1.5}
    \centering
    \begin{tabular}{p{4cm}|p{8cm}}
    \hline
        Tool & Anwendung im Experiment \\ \hline
        Minikube & Lokales Kubernetes \\ 
		Podinfo & Minimale Webapi \\
		NGINX Ingress Controller & API Gateway \& Loadbalancer \\
		Helm &  Package Manager für Kubernetes \\ 
		Locust & Testet Performance von APIs \\ \hline 
    \end{tabular}
    \caption{Verwendete Tools und ihre Anwendung}
    \label{tab:tools}
\end{table}

Im Experiment wurde zur einfachen Veranschaulichung lediglich eine API nach Kubernetes publiziert.
Die dazu gehörige \gls{yaml} Datei ist im \autoref{chap:anhangB} unter \autoref{lst:apiyaml} zu finden.
Für den Ingress Controller wurde der Namespace \textit{nginx-api} in Kubernetes angelegt.
Anschließend wurde ein Ingress Controller in diesen Namespace installiert.
Die \gls{yaml} Datei \autoref{lst:ingresscontrolleryaml} zeigt hierbei Konfigurationsdefinitionen für die Erstellung einer Rate Limiting Richtlinie auf diesen Controller.

Die verwendeten Parameter zur Ratenbegrenzung waren hierbei \textit{rate}, \textit{key} und \textit{zoneSize}.
Der Variable \textit{zoneSize} wurde mit einem \gls{mb} einen geteilten Speicher zugewiesen, welcher den Status von bis zu 16.000 \gls{ip}-Adressen erfassen kann.   
Insgesamt wurde festgelegt, dass für jede Anfrage, welche dieselbe ID im Cookie hat, zwei Anfragen pro Sekunde an die API stellen darf.
Die ID in den Cookies simulieren in diesem Experiment die IP-Adressen der einzelnen Benutzer.
Für den \textit{key} kann auch mit \textit{binary\_remote\_addr} nach IP-Adressen gefiltert werden. \\

\begin{lstlisting}[caption={Code Auszug aus der QuickstartUser Klasse},label={lst:locustscriptcut},captionpos=b,style=java, xleftmargin=0.05\textwidth,xrightmargin=0.05\textwidth, firstnumber=12][H]
wait_time = constant(0.1)

def on_start(self):
    global user_amount
    user_amount += 1
    self.cookie = { "id" : str(user_amount) }

@task
def visit_api(self):
    with self.client.get("/", headers={"Host": "api.rate.com"}, cookies = self.cookie,  timeout=0.2, catch_response=True) as response: 
        status_code = response.status_code
        if status_code == 503: 
            response.failure(str(status_code) + " Service Temporarily Unavailable")  
        elif response.request_meta["response_time"] > 200: 
            response.failure("API failed.") 
        else:
            response.success()
\end{lstlisting}

Die Erstellung des Locust Dienst wird mit der Datei aus \autoref{lst:locustyaml} definiert.
\autoref{lst:locustscriptcut} zeigt einen Auszug der Klasse \textit{QuickstartUser} aus \autoref{lst:locustscript}, welches ein Pythonscript ist und vom Locust Dienst ausgeführt wird.
Die Klasse \textit{QuickstartUser} simuliert einen Benutzer, welcher eine Anfrage an den Ingress Controller stellt. 
Wenn der Response länger als 200 ms benötigt, wird die Anfrage abgebrochen und die Fehlermeldung \textit{API failed} geloggt.
Wenn auf dem Ingress Controller eine Rate Limit Strategie greift und der Response den Fehlercode 503 enthält, wird die Fehlermeldung \textit{503 Service Temporarily Unavailable} geloggt.
Nachdem der Benutzer eine Antwort auf seinen Request erhalten hat, führt er diese Aufgabe nach 100 ms erneut aus, welches die Variable \textit{wait\_time} aus Zeile 12 definiert.
Über den Locust Client kann die Anzahl der Benutzer definiert werden, welche alle parallel diese Aufgabe ausführen.

\section{Durchführung}
\label{sec:durchfuehrung}
Vorab wurde die API bzw. der Ingress Controller ohne implementierte Rate Limiting Strategie getestet.
Die daraus gewonnenen Ergebnisse dienen als Referenzdaten.
Außerdem soll allgemein die Performancelast der API getestet werden, um abschätzen zu können, wie viele Benutzer zeitgleich auf die API zugreifen können.

Der Performancetest wurde über die Website vom Locustdienst ausgeführt.
Wie \autoref{fig:loctestkonfig} zu entnehmen ist, wurden jede Sekunde zehn Benutzer instanziiert bis insgesamt 600 Benutzer zeitgleich aktiv sind, welche jede Sekunde jeweils bis zu zehn Anfragen an den Ingress Controller stellen.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{image/chart1.PNG}
	\caption{Liniendiagramm zu RPS und Fehler pro Sekunde ohne Rate Limit Strategie}
	\label{fig:chart1}
\end{figure}

\autoref{fig:chart1} zeigt die Ergebnisse in einem Liniendiagramm, wo auf der horizontalen Achse die zeitliche Komponente und auf der vertikalen Achse die \gls{rps} definiert sind.
Die hier detektierten Fehler sind Anfragefehler, welche länger als 200 ms benötigen.
Anschließend wurde ein weiterer Testlauf auf einen Ingress Controller mit implementierter Rate Limiting Strategie durchgeführt.
Die Ergebnisse von diesem Durchlauf sind der \autoref{fig:chart2} zu entnehmen.
Hier sind die Fehler sowohl Anfragefehler als auch auch Rate Limiting Fehlermeldung mit dem Statuscode 503. 

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{image/chart2.PNG}
	\caption{Liniendiagramm zu RPS und Fehler pro Sekunde mit Rate Limit Strategie}
	\label{fig:chart2}
\end{figure}

Da anhand der Daten aus den beiden Testläufen abzulesen ist, dass das Rate Limit erfolgreich implementiert wurde, wurden noch weitere Tests mit unterschiedlichen Benutzerzahlen durchgeführt.
Dadurch soll der Performanceeinfluss, den das Rate Limit Feature auf die API hat, gemessen werden.

\begin{table}[H]
    \setlength{\arrayrulewidth}{0.5mm}
    \setlength{\tabcolsep}{18pt}
    \renewcommand{\arraystretch}{1.5}
    \centering
    \begin{tabular}{l|r|r|c|c}
    \hline
        User & PF\_none & PF\_rated & RLF & Time (ms) \\ \hline
        200 & 6603 & 3056 & 78035 & 120   \\ 
        400 & 91735 & 39014 & 59857 & 330   \\ 
        600 & 98840 & 51269 & 48259 & 610   \\ 
        800 & 99935 & 88015 & 13455 & 930   \\  \hline 
    \end{tabular}
    \caption{Ergebnisse zur Durchführung bis 100.000 Anfragen mit unterschiedlichen Benutzerzahlen}
    \label{tab:data}
\end{table}
Hierbei wurden insgesamt acht Testdurchläufe mit 200, 400, 600 und 800 Benutzern durchgeführt. 
In diesen Testphasen sind die Benutzer mit einer Spawnrate von 200 Benutzern pro Sekunde festgesetzt worden.
Pro Testfall wurden insgesamt 100.000 Requests durchgeführt. 
Die Konfiguration wurde von zwei auf einen zugelassener Request pro Sekunde für die Benutzer angepasst, um deutlich unterscheidbarere Werte zu erhalten. 
In \autoref{tab:data} sind die gesammelten Daten aus den Tests mit unterschiedlichen Benutzerzahlen dargestellt. 
PF steht hierbei für Performance Fails, welche auftreten, wenn die Anfrage mehr als 200 ms benötigt.
Der Suffix none steht für den Ingress Controller ohne implementiertem Rate Limit Feature, während der Suffix rated für den mit der Rate Limit Strategie implementierten Ingress Controller steht.
RLF steht für Rate Limiting Fails, welche ausschließlich die vom Rate Limit Feature publizierten 503 Fehlercodes sind.
Die durchschnittliche Antwortzeit in Millisekunden ist für den Ingress Controller mit und ohne implementiertem Rate Limit Feature für die jeweiligen Benutzerzahlen fast identisch, weshalb diese in der Tabelle zusammengefasst sind.


\section{Beobachtung}
\label{sec:beobachtung}
Beim ersten Testlauf ist, wie \autoref{fig:chart1} zeigt, zu erkennen, dass in den ersten 20 Sekunden alle Anfragen erfolgreich und keine Fehlermeldungen entstanden sind.
Bei der verwendeten Erzeugungsrate von zehn Benutzern pro Sekunde stellen somit aktiv 200 Benutzer mehrere Anfragen pro Sekunde an den Controller.
Ab knapp 700 \gls{rps} und immer weiter steigender Benutzerzahl kommt es vermehrt zu Fehlermeldungen, da die Antwortzeiten der Anfragen über 200 ms benötigen.
Bereits nach 400 Benutzern ist der Controller so ausgelastet, dass kaum noch Anfragen unter 200 ms abarbeitet werden können. 

Da beim zweiten Testdurchlauf ein Rate Limit von zwei Anfragen pro Benutzer festgelegt ist, treten hier direkt zu Anfang der Testphase bereits Fehlermeldungen auf, wie \autoref{fig:chart2} zeigt.
Diese besitzen jedoch ausschließlich den Statuscode 503 und werden aufgrund der Überschreitung des Rate Limits ausgelöst. 
Aber auch in diesem Testdurchlauf kommen ab einer Benutzerzahl von über 400 vermehrt Fehlermeldungen, dass die Antwortzeit der API zu lange benötigt. 

Bei den diversen Performancetests mit unterschiedlicher Benutzerzahl geht aus \autoref{tab:data} hervor, dass deutlich weniger PFs auftreten bei implementiertem Rate Limiting Feature.
Außerdem ist zu beobachten, dass bei steigender Benutzerzahl, sowohl die Antwortzeiten als auch die PFs sich immer weiter erhöhen.
Im Vergleich dazu nimmt bei steigender Benutzerzahl die Rate Limit Fehlermeldungen immer weiter ab.