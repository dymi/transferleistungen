# Meetings:

## Fr. 09.12.22 - Projektvorstellung und Abstimmung mit Ole
- [ ]  Aktuellen Stand des [Projekts](https://gitlab.com/dymi/ci-sec/-/tree/master) zeigen und erklären
  - gitlab.yml -> wasttesting -> waitforit -> reports
  - .linux für lokalen gitlab-runner
  - bisherige Pitfalls
  - UMGANG mit error code ?
  - Vergleich zum base repo; bisherige verbesserungen
  - Cachingstrategien? 
  - Scantime; manuelle Scanprozesse

- [ ] BA Arbeit
  - Dipl. Titel?
  - Freitag 13Uhr Serientermin für 30min und Abgabe von Sachen die reviewt werden können?
  - Rückmeldung bis spätestens nächsten Termin
  - Montag 12.12 schick ich erste Version. Heute und am WE nochmal glatt machen und SecurityTesting durchballern
  - 23., 30. 06. anwesend?