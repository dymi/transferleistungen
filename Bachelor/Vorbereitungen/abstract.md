# Themen:
## DevSecOps -> Vulnerabilty scanner
* [Breachlock](https://www.breachlock.com/devsecops-best-practices-for-vulnerability-scanning/)
![](images/devsecops.PNG)

* [Paper - Classification of Software Security Tools](http://ceur-ws.org/Vol-2933/paper28.pdf)
* [Demo-Targets](https://github.com/secureCodeBox/secureCodeBox/tree/main/demo-targets)

## Integrated Dynamic Security Testing Tools:
[Paper](https://sci-hub.se/10.1109/EDOC49727.2020.00026)

In this paper, we studied the integration of continuous
(dynamic) security testing into CI/CD pipelines. To our knowledge, our work provides the first academic view on the topic.
We defined eight requirements for a proper adaptation of
automated dynamic application security testing for DevSecOps
teams. These requirements ensure practical and agile development of web applications, web services and alike. In order to
identify the practical challenges in meeting these requirements,
we performed a case study by integrating three commonly
known security testing tools into a CI/CD pipeline. We believe
that the interested DevSecOps teams can benefit from our
work as they can use our approach as a reference architecture
for dynamic testing in CI/CD pipelines and learn from the
challenges/solutions we outlined.

Forschungsfrage: How can we intefrate DAST into CI/CD and ensure that DevOps requirements are met?
  * (1) identify the requirements for successful integration of security testing tools
  * (2) describe the tools that were chosen for the integration and discuss their integration into CI/CD
  * (3) investigate the performance of our implementation in a CI/CD pipeline and 
  * (4) provide an overview of the challenges that were encountered during implementation

DevSecOps goals (1) :
* Quick Build Times
* Parallel pipline jobs
* Testing of multiple versions
* Test every commit
* Only Build what is necessary
* Flexible deployment strategies
* Report vulnerabilities
* Flexibility of testing technology 

Challanges:
![](images/challanges.PNG)

# Mein Thema -> Dynamic Application Security Testing: Integration von Sicherheitstests in CI/CD Pipelines:

## Forschungsfrage:
* Wie können DAST in bestehende CI/CD Pipelines integriert werden?
* Welche DevOps Anforderungen gibt es, die DAST bestehen müssen?
* Welche DAST-Tools gibt es?
* In welchen Stages lässt sich DAST in der CI/CD am Besten integrieren?
* Welche Herausforderungen sind mit DAST für das Entwicklerteam verbunden?
* Ist DAST Entwickler, DevOps oder Sec Aufgabe? -> Wer kümmert sich drum?
* Wie stark ist die Performance der CI/CD Pipeline betroffen?

Struktur:
* Einleitung
  * Problemstellung
  * Lösungsansatz
  * Forschungsfragen
  * Aufbau der Arbeit
* Related Work
* Grundlagen
  * DevOps
    * SDLC bzw. CI/CD
    * Docker
  * Security Testing
    * Static
    * Dynamic
  * DAST Techniques
    * Web Application Security Testing (WAST)
    * Security API Scanning (SAS)
    * Behaviour Driven Security Testing (BDST)
  * Auswahl der DAST Tools
* Experiment
  * Aufbau
  * Durchführung
  * Beobachtung
* Diskussion
* Fazit und Ausblick
