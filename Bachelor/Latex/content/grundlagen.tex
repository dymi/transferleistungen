\chapter{Grundlagen}
\label{chap:grundlagen}
In diesem Kapitel werden die beiden Forschungsfragen \textbf{F1} und \textbf{F2} beantwortet.
Insbesondere werden Grundlagen zu \gls{devsecops} sowie \gls{cicd} und der Virtualisierungssoftware Docker geschaffen.
In \autoref{sec:securitytesting} werden dynamische von statischen Sicherheitstests abgegrenzt und drei verschiedene \gls{dast}-Techniken erklärt.
Im Anschluss werden unter \autoref{sec:dasttools} die verschiedenen Tools vorgestellt, welche in der Fallstudie verwendet werden.

\section{DevSecOps}
\label{sec:devops}
\gls{devops} beschreibt eine Reihe von Praktiken aus der Kombination aus der Softwareentwicklung (\textit{Development}) und diverse IT-Geschäftstätigkeiten (\textit{Operations}).
Während im Bereich der Softwareentwicklung lediglich die Software für den Kunden entwickelt wird, tragen die weiteren IT-Geschäftstätigkeiten dazu bei, die Software dem Kunden zur Verfügung zu stellen.
Gartner definiert IT-Operations als die Mitarbeiter und Managementprozesse, die mit dem IT-Service-Management verbunden sind, um den Kunden die richtige Reihe von Services in der richtigen Qualität und zu wettbewerbsfähigen Kosten bereitzustellen \autocite{gartner_definition_nodate}.

Das Ziel von \gls{devops} ist es, den \gls{sdlc} zu verkürzen und mittels kontinuierlicher Bereitstellung des Produkts, die Softwarequalität zu erhöhen \autocite{loukides_what_nodate}.
Außerdem unterstützt \gls{devops} die Praxis der agilen Softwareentwicklung \autocite{mohammad_devops_2017}.

Wie verschiedene Forschungsarbeiten zeigen, gibt es keine einheitliche und klare Definition über den Term \gls{devops} \autocite{dyck_towards_2015,jabbari_what_2016,erich_qualitative_2017}.
Verschiedene Modelle wie \gls{cams} zeigen jedoch einen gemeinsamen Satz aus den gleichen Prinzipien wie der Kultur, Automatisierung, Messungen und das Teilen von Verantwortlichkeiten \autocite{maroukian_leading_2020}.
In der Forschungsarbeit von Muñoz et al. wird \gls{devops} als Kombination von spezifischen Praktiken, kultureller Einstellung und Tools definiert \autocite{munoz_guidance_2021} .
Bass et al. haben in ihrem Buch \textit{DevOps: A Software Architect's Perspective} jedoch folgende Definition aufgestellt, welche auch in dieser Arbeit als Definition für \gls{devops} verwendet wird:

\begin{quote}
	\textit{DevOps is a set of practices intended to reduce the time between
		committing a change to a system and the change being placed into
		normal production, while ensuring high quality.} \autocite[22]{bass_devops_2015}
\end{quote}

Während viele Ideen und Ansätze der \gls{devops} Praktiken aus den Werten des agilen Manifests stammen \autocite{martin_principles_2001}, welches unter anderem von Robert C. Martin definiert wurde, wird die Sicherheit der Software oftmals vernachlässigt oder erst in einer späteren Entwicklungsphase integriert \autocite{khan_systematic_2021}.
Obwohl Sicherheit ein Qualitätsmerkmal ist \autocite{iso_isoiec_nodate}, wird das automatisierte Testen der Software mittels Sicherheitstests nicht als selbstverständlich erachtet im Sinne der \gls{devops} Praktiken.
Daher ist die Integration von Sicherheit in diesen Praktiken essentiell, um ein qualitativ hochwertiges sowie sicheres Softwareprodukt dem Kunden bereit zu stellen.
\gls{devsecops} bezeichnet somit die explizite Integration von Sicherheitspraktiken in den bestehenden \gls{devops} Prozess.

Die verschiedenen Techniken, um Sicherheit einer Software zu testen, werden in \autoref{sec:securitytesting} dargestellt.
Da der Fokus dieser Arbeit auf Sicherheitstests beruht, wird ausschließlich der Term \gls{devsecops} verwendet, welcher stets alle \gls{devops} Praktiken beinhaltet.

\subsection{CI/CD}
\label{sec:cicd}
Einer der Hauptpraktiken von \gls{devsecops} ist die Workflow-Automatisierung.
Um Sicherheitstests zu automatisieren, werden unter anderem \gls{cicd}-Pipelines verwendet.
Die Idee von \gls{ci} wurde im Jahr 2000 von Martin Fowler vorgestellt \autocite{fowler_continuous_2000} und später um das Konzept zur Softwareauslieferung mittels \gls{cd} erweitert \autocite{humble_continuous_2010}.
\gls{cicd} ermöglicht das automatisierte Bauen, Testen und Ausliefern der Software.
Ein \gls{cicd}-Dienst kompiliert die inkrementellen Codeänderungen, die von Entwicklern vorgenommen werden, verknüpft und packt sie dann in Software-Ergebnisse.
Anschließende automatisierte Tests verifizieren die Softwarefunktionalitäten und die automatisierte Bereitstellungsdienste liefern die Software an den Endbenutzer.
Ziel ist es, die Früherkennung von Fehlern zu verbessern, die Produktivität zu steigern und schnellere Release-Zyklen bereitzustellen. \autocite{chen_continuous_2015}

Voraussetzung einer \gls{cicd}-Pipeline ist die Verwaltung und Pflege eines Code-Repositorys mit Versionskontrollmechanismen.
Durch die Versionskontrolle können Änderungen an Dokumenten bzw. dem Code nachvollzogen und verfolgt werden \autocite[414]{zolkifli_version_2018}.
Außerdem bietet es die Möglichkeit des kollaborativen Arbeitens, indem unterschiedliche Personen sich eigene lokale Kopien des Repositorys erstellen und dort Änderungen vornehmen, ohne die aktuellen Anpassungen von Anderen zu beeinflussen \autocite[414]{zolkifli_version_2018}.
Das Tool Git ist eine Möglichkeit, den eigenen Code zu versionieren \autocite{chacon_pro_2014}.
Da in der Softwareentwicklung Git am weitesten verbreitet ist und renommierte \gls{devops}-Plattformen wie \textit{GitLab}, \textit{GitHub} und \textit{Azure DevOps} auf Git aufbauen, wird in dieser Arbeit ebenfalls dieses Tool verwendet.

Die Versionierung erfolgt mit Hilfe von baumartigen Strukturen.
Die Hauptversion des Codestandes wird in einem Main- bzw. Master-Branch nachgehalten.
Von diesem Branch können lose und entkoppelte Kopien erschaffen werden, auf denen neue Codeänderungen getestet werden können, ohne dabei den Stand vom Master-Branch zu beeinflussen.
\autoref{fig:gitbranchingtmp} zeigt hierbei das Branch Modell mittels eines beispielhaften Aufbaus, welches das Trunk Based Development Modell verwendet.
Der Fokus liegt bei diesem Modell auf kurzlebige Feature-Branches und wird als Best Practice empfohlen \autocite{zettler_trunk-based_nodate, atlassian_gitflow_nodate}.
Neben diesem Modell gibt es auch noch weitere Workflow-Modelle wie Git-, GitHub- und GitLab-Flow, welche sich in ihren Konventionen bzgl. der Verwendung und Abzweigungen von Branches unterscheiden \autocite{bunyakiati_open-source_2020}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{image/trunkbasedgit.PNG}
	\caption{Aufbau eines Trunk Based Development Modells}
	\label{fig:gitbranchingtmp}
\end{figure}

Mittels \textit{Commits} können lokale Codeänderungen auf dem jeweiligen Entwickler-PC auf den aktuellen Branch versioniert werden.
Über \textit{Push}-Befehle werden alle hinzugefügten Codeänderungen vom lokalen Branch auf den Remote-Server übertragen.
Der Remote-Server kann auf diese \textit{Push}-Befehle reagieren, indem er einen 
automatisierten Prozess anstößt.
Dieser Prozess kann divers konfiguriert werden.
Im Sinne von \gls{ci} werden hierbei mindestens die beiden Phasen \textit{Build} und \textit{Test} ausgeführt.

Beim \textit{Build} wird der Code der jeweiligen Software kompiliert, um festzustellen, ob der Code ohne Fehler starten kann.
Die \textit{Test}-Phase startet alle implementierten Unit-Tests.
Unit-Tests sind hierbei Regressionstests, welche Algorithmen von kleinen Softwareeinheiten nach vordefinierten Szenarien testet und damit sicherstellt, dass diese Algorithmen den Erwartungen entsprechend funktionieren.
Außerdem können diese dabei helfen, bei Quellcodeanpassungen zu verifizieren, dass keine ungewünschten Nebeneffekte auftreten.
Bei Fehlschlägen in den jeweiligen Phasen können Regeln definiert werden, sodass dieser Codestand z.B. nicht mit den Master-Branch zusammengeführt werden darf.
Des Weiteren werden i.d.R. die jeweiligen Verursacher und Repositoryverantwortliche über fehlerhafte Buildstatus über Notifikationen wie E-Mail in Kenntnis gesetzt.
Diese Prozesse werden alle über einzelne Pipeline-Jobs definiert. 

Als Best Practice wird empfohlen, dass unabhängig vom Branch der Code durch die Pipeline gebaut und getestet wird.
Um frühzeitiges Feedback zu erhalten, lange Wartezeiten zu ersparen und Prozesse nicht zu blockieren, wird empfohlen, dass die Phasen in einer Pipeline möglichst schnell und ggf. parallel ausgeführt werden \autocite{dileepkumar_optimize_2021}.
Im \gls{ci}-Prozess werden oftmals fertig gebaute Softwarepakete als Artefakte über ZIP-Dateien bereitgestellt.
Diese werden verwendet, um auf Test- oder Produktivumgebungen ausgeliefert zu werden.
Im \gls{cd}-Prozess werden die einzelnen Schritte zur Auslieferung und Inbetriebnahme der Software definiert.

Einer der Hauptvorteile der Verwendung von \gls{cicd}-Prozessen ist die Automatisierung von vorgefertigten Prozessen, welche den \gls{sdlc} deutlich beschleunigen und für kontinuierliches Feedback auch an die Entwickler beiträgt.
Durch die Verwendung von verschiedenen Ausführungsumgebungen und das Arbeiten mit versionierten Ständen bzw. Branches, ist sichergestellt, dass eine funktionierende Hauptversion der Software existiert.

Im Gegensatz zu den Vorteilen, stellt die Erstellung insbesondere von automatisierten Testfällen und der entsprechenden Umgebung eine umfangreiche Herausforderung dar.
Speziell bei sich stetig ändernden und neuen Anforderungen kann dies zu komplexen Codeänderungen und -erweiterungen führen.
Die Erstellung von Build-Systemen kann ebenfalls durch wachsende Anforderungen und neu hinzugefügten Diensten stark an Komplexität zunehmen und kann es erschweren, einen flexiblen und modularen Build-Prozess zu implementieren.

Das Kompilieren und Ausführen von Code benötigt Tools, welche dem Build-Server mitgegeben bzw. installiert werden müssen, damit dieser die entsprechenden Aufgaben ausführen kann.
Damit der Build-Server nicht für jedes Coderepository die benötigten Tools installieren muss, wird eine leichtgewichtige Virtualisierungsumgebung geschaffen, worauf die jeweiligen Pipeline-Phasen ausgeführt werden.
Diese Virtualisierung wird meist mit dem Tool Docker realisiert.

\subsection{Docker}
\label{sec:docker}
Docker ist eine in Go implementierte Virtualisierungssoftware, welche 2013 erstmalig veröffentlicht wurde.
Docker ist eine Reihe von \textit{Platform as a Service} Produkten, welche eine Virtualisierung auf Betriebssystemebene verwenden, um Software in Paketen bereitzustellen, die als Container bezeichnet werden.
Der Dienst, der die Container bereitstellt, wird als \textit{Docker Engine} bezeichnet. \autocite{docker_docker_2023}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.95\linewidth]{image/dockerflow.PNG}
	\caption{Übersicht der wichtigsten Docker Komponenten und deren Kommunikationswege}
	\label{fig:docker1}
\end{figure}

Container sind voneinander isoliert und bündeln ihre eigene Software, Bibliotheken und Konfigurationsdateien.
Sie können jedoch über definierte Kanäle miteinander kommunizieren.
Im Vergleich zu virtuellen Maschinen, teilen sich alle Container die Dienste eines einzigen Betriebssystemkerns, weshalb diese deutlich weniger Ressourcen verwenden \autocite{potdar_performance_2020}.

Ein besonderer Vorteil ist, dass Docker die Anwendung und ihre Abhängigkeiten in einem virtuellen Container verpacken kann, der auf jedem Linux-, Windows- oder macOS-Computer ausgeführt werden kann.
Dies ermöglicht es, die zu erstellende Software auf jedem Server auszuführen, wo Docker installiert ist.

Die Docker Software lässt sich in die drei Komponenten Docker Daemon, Docker Engine API und Docker \gls{cli} aufteilen.
Der Docker Daemon ist ein persistenter Prozess, der die Docker Container und Container Objekte verwaltet.
Dieser reagiert auf Anfragen, welche an die Docker Engine API gestellt werden.
Als Client-Programm dient \textit{docker}, welches ein \gls{cli} bereitstellt und den Nutzer erlaubt mit den Docker Daemon zu kommunizieren.

Die drei Hauptklassen von Docker Objekten sind Images, Containers und Services.
Container sind standardisierte, gekapselte Umgebungen, in der Anwendungen ausgeführt werden. 
Ein Container wird über die Docker API oder \gls{cli} verwaltet.
Ein Docker Image ist eine schreibgeschützte Vorlage, die zum Erstellen von Containern verwendet wird. Diese Images werden außerdem zum Speichern und Versenden von Anwendungen verwendet.
Ein Docker Service ermöglicht die Skalierung von Containern über mehrere Docker Daemons hinweg, welches auch als Docker Swarm bezeichnet wird.

Die Docker Registry ist ein Repository für Docker Images.
Dort können Images sowohl hoch- als auch heruntergeladen werden.
Die Hauptregistry ist Docker Hub.
Es können aber auch private Repositories registriert werden, welche im Unternehmenskontext häufig verwendet werden, damit eine vertrauenswürdige und eigene Quelle zu Docker Images besteht.

Docker Compose ist ein Tool zum Definieren und Ausführen von Docker Anwendungen mit mehreren Containern.
Es verwendet YAML-Dateien, um die Dienste der Anwendung zu konfigurieren und führt den Erstellungs- und Startvorgang aller Container mit einem einzigen Befehl aus.
Die \textit{docker-compose} \gls{cli} ermöglicht es Benutzern Befehle auf mehreren Containern gleichzeitig auszuführen, wie z.B. die Erstellung von Images sowie Skalierung und Ausführung von Containern.

Detailliertes Verständnis hinter den Docker Prozessen und der Definition von Docker Images sowie das Erstellen von Containern und die Kommunikation untereinander, ist in dieser Fallstudie essentiell.
Aufgrund der Zusammenhänge, der Komplexität und zugeschnittenen Szenarien werden auf weitere Docker Spezifikationen in \autoref{chap:experiment} und \autoref{chap:diskussion} direkt eingegangen.

\section{Open Web Application Security Project (OWASP)}
\label{sec:owasp}
\gls{owasp} ist eine von der Community betriebene Non-Profit-Organisation, dessen Hauptziel es ist, aktuelle Schwachstellen in modernen Webanwendungen zu untersuchen.
Sie erstellt frei verfügbare Artikel, Dokumentationen, Methodiken und Tools im Bereich der Sicherheit von Webanwendungen.
Die Stiftung stellt eine Reihe von Standards vor, wie die Ermittlung des Schweregrads der Schwachstellen, ihrer möglichen Ursachen und wie man diese mindern kann.
Die \gls{owasp} Top 10 2021 ist das Ergebnis der aktuellsten Forschung auf der Grundlage umfassender Daten, die von diversen Partnerorganisationen zusammengestellt wurde. \autocite{owasp_owasp_2023}

\autoref{tab:owasp10} zeigt hierbei die Übersicht der \gls{owasp} Top 10 2021.
Diese Liste ist in den meisten Sicherheitstools integriert und dient als Referenz, um nach konkreten Schwachstellen zu suchen und bei gefundenen Schwachstellen auf die entsprechende Ursache sowie Lösung hinzuweisen.
Über die von \gls{owasp} bereitgestellten Dokumentationen dieser Schwachstellen werden umfangreiche Berichte von den jeweiligen Tools erstellt und verweisen teils auf die Dokumentation direkt.  
Alle in dieser Arbeit verwendeten Sicherheitstools referenzieren auf diese Liste. 

\begin{table}[H]
	\centering
		\begin{tabular}{|c|l|}
			\hline
			\textbf{Schwachstelle} & \textbf{Beschreibung}          \\ \hline
			A01           & Broken Access Control                      \\ \hline
			A02           & Cryptographic Failures                     \\ \hline
			A03           & Injection                                  \\ \hline
			A04           & Insecure Design                            \\ \hline
			A05           & Security Misconfiguration                  \\ \hline
			A06           & Vulnerable and Outdated Components         \\ \hline
			A07           & Identification and Authentication Failures \\ \hline
			A08           & Software and Data Integrity Failures       \\ \hline
			A09           & Security Logging and Monitoring Failures   \\ \hline
			A10           & Server-Side Request Forgery                \\ \hline
		\end{tabular}	
	\caption{Übersicht der OWASP Top 10 2021}
	\label{tab:owasp10}
\end{table}

\section{Penetrationstests}
\label{sec:penetrationstests}
Ein Penetrationstest ist eine proaktive und autorisierte Aufgabe, um die Sicherheit eines IT-Systems zu durchbrechen \autocite{kothia_knowledge_2019}.
Es werden von einer vertrauenswürdigen Person Angriffsansätze verwendet, welche von Hackern und feindlichen Eindringlingen gleichermaßen verwendet werden.
Diese Penetrationstests schützen das Unternehmen vor Fehlern indem sie Aufsichtsbehörden, Interessenvertretern und Kunden die gebotene Sorgfalt und Compliance nachweisen.
Unternehmen sollten regelmäßige Penetrationstests durchführen, um die Schwachstellen und kritischen Risiken bei Prozessen, Technologien und Menschen zu identifiziert. \autocite{bacudio_overview_2011}. 

Manuelle Penetrationstests lassen sich zusammenfassend in die folgenden drei Phasen einteilen, wobei hier bewusst die englischen Fachwörter aus diesem Bereich verwendet werden \autocite[510]{chowdhary_autonomous_2020}:
\begin{itemize}
	\item[\textbf{P1}] \textit{Scoping \& Reconnaissance} 
	\item[\textbf{P2}] \textit{Vulnerability Analysis}
	\item[\textbf{P3}] \textit{Exploitation \& Reporting} 
\end{itemize} 

Die Phase \textbf{P1} dient der Informationsbeschaffung.
Es wird ein Bereich für die Sicherheitsbewertung definiert und das Netzwerk gescannt. 
Reconnaissance-Tools wie \textit{nmap} helfen hierbei Informationen über die Zielumgebung zu erfassen \autocite{shah_penetration_2019}. 
Dadurch können offene Ports erkannt und öffentlich zugängliche Dienste ermittelt werden.
Diese Ports können anschließend intensiver und nach allgemein bekannten Schwachstellen gescannt werden.
Diese Schwachstellenanalyse wird in \textbf{P2} durchgeführt.
Durch den Einsatz von verschiedenen Schwachstellen Scannern können bekannte Schwachstellen der Zielumgebung nachgewiesen werden. 
Der Netzwerkdatenverkehr kann durch Tools wie Burpsuite oder ZAP analysiert und nach weiteren Schwachstellen vom Pentester untersucht werden.  
In \textbf{P3} werden die gefundenen Schwachstellen ausgenutzt. 
Pentester verwenden benutzerdefinierte Skripte oder Tools wie Metasploit, um diese Schwachstellen anzugreifen. 
In dieser Phase werden die Daten gesammelt und ausführliche Berichte erstellt, welche die Ergebnisse im Detail erläutern. \autocite[510]{chowdhary_autonomous_2020}

In dieser Arbeit werden dynamische Sicherheitstests erstellt und automatisiert in einer isolierten Umgebung ausgeführt.
Da die zu testenden Dienste vorab bekannt sind, findet in dieser Arbeit die Phase \textbf{P1} keine Anwendung. 

\section{Security Testing}
\label{sec:securitytesting}
Security Testing in der Softwareentwicklung beschreibt verschiedene Testverfahren und Methodiken, um Sicherheitslücken zu identifizieren.
In der Literatur werden Whitebox- von Blackbox-Tests unterschieden.
Whitebox-Tests sind Tests, wo der Quellcode der Anwendung zur Verfügung steht.
Als Kontrast dazu sind die Blackbox-Tests Solche, welche den Einblick auf den Quellcode oder die interne Systemstruktur nicht zulassen.
Manche Literaturen verwenden auch den Term \textit{Greybox} Tests, wo eine Mischform aus White- und Blackbox-Testverfahren verwendet wird. \autocite{nidhra_black_2012}

Die meisten Web- und Cloudanwendungen können über die Dienst-, Infrastruktur- und Plattformebene nach Sicherheitslücken getestet werden \autocite{zech_risk-based_2011}.
Diese Arbeit behandelt ausschließlich die Sicherheitstests auf Dienstebene.
Es werden hauptsächlich zwischen den beiden Verfahren \gls{sast} und \gls{dast} unterschieden, welche in diesem Kapitel näher betrachtet werden.

\subsection{Static Application Security Testing (SAST)}
\label{sec:static}
\gls{sast} definiert ein Whitebox-Testverfahren, welches Quellcode nach konkreten Mustern zu anfälligen Schwachstellen analysiert \autocite{yang_towards_2019}.
Entsprechende Tools können in die integrierte Entwicklungsumgebung mit aufgenommen werden, so dass Entwicklern sofortiges Feedback zu Codeschwachstellen mitgeteilt werden kann.
Außerdem bieten die meisten Tools die Möglichkeit, die Prozesse zu automatisieren und auch in \gls{cicd} zu integrieren.
Umfangreiche Berichterstellung helfen den Entwicklern nicht nur mögliche Schwachstellen sondern auch Bugs und unkonventionellen Code zu identifizieren.
Hierbei werden die exakten Codezeilen zum Befund und mögliche Lösungen angezeigt.

Einer der renommiertesten Tools zur statischen Codeanalyse ist \textit{SonarCloud} \autocite[171]{ciancarini_survey_2020}.
Dieses Tool ist ein cloudbasierter Dienst, welcher bis zu 25 verschiedene Programmiersprachen nach diversen Codingstandards analysiert.
\textit{SonarCloud} kann an die Repositories von den vier \gls{devops} Plattformen \textit{GitHub}, \textit{Bitbucket Cloud}, \textit{Azure DevOps} und \textit{GitLab} angebunden werden \autocite{sonarcloud_sonarcloud_2022}. 

\autoref{fig:sonar1} zeigt beispielhaft anhand des \gls{owasp} \gls{zap} Projekts eine Übersicht der folgenden sechs Kennzahlen: \textit{Bugs}, \textit{Code Smells}, \textit{Vulnerabilities}, \textit{Security Hotspots}, \textit{Code Coverage} und \textit{Code Duplications}.
Anhand dieser Werte können \gls{qa}- sowie Sicherheitsbeauftrage und besonders Entwickler den geschriebenen Code nach diesen verschiedenen Kategorien nachbessern.
Durch diese Analysen soll sich nicht nur die Sicherheit sondern auch die Qualität des Codes und auch des Softwareprodukts erhöhen.
\textit{SonarCloud} unterstützt die Überarbeitung der gefundenen Codestellen indem das Problem geschildert und mögliche Lösungsansätze aufgezeigt werden.
\autoref{fig:sonar3} zeigt die \textit{Security Hotspots} von \gls{zap}.
Während auf der linken Seite die gefundenen \textit{Security Hotspots} nach Priorität sortiert sind, wird auf der rechten Seite die gefundene Codestelle dargestellt.
Die Reiter wie \textit{What's the risk?} und \textit{How to fix it?} erklären das Problem und stellen mögliche Lösungsansätze bereit.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.95\linewidth]{image/sonarzap3.PNG}
	\caption{\textit{SonarCloud} Übersicht der \textit{Security Hotspots} \autocite{sonarcloud_owasp_2023-1}}
	\label{fig:sonar3}
\end{figure}

\textit{SonarCloud} steht allen Open-Source-Projekten frei zur Verfügung, während es für private Repositories kostenpflichtig ist.
Dieser Dienst wird bei \textit{LichtBlick} für alle privaten Repositories bereits verwendet.

\subsection{Dynamic Application Security Testing (DAST)}
\label{sec:dynamic}
\gls{dast} wird als ein Testprozess gegen eine laufende Anwendung oder Softwareprodukts während des Betriebes bezeichnet \autocite{techopedia_what_2014}.
Sowohl White- als auch Blackboxtests sind über \gls{dast} möglich.
Angriffsszenarien werden als Testfälle mittels speziell konfigurierten Anfragen konstruiert und gegen die laufende Anwendung gestellt \autocite{hsu_practical_2019}.
Die Herausforderung ist hierbei die Erstellung der korrekten Anfrage, welche durch die Auswertung der erhaltenen Informationen identifizieren kann, ob eine Sicherheitslücke besteht \autocite{rangnau_continuous_2020}.
Als Softwareanwendung wird in dieser Arbeit eine über \textit{GitHub} zugängliche Open-Source-Anwendung verwendet \autocite{noauthor_owasp_2023}.
Somit werden in dieser Arbeit hauptsächlich Whitebox-Tests verwendet.
Die drei folgende \gls{dast}-Techniken werden in dieser Arbeit verwendet: \gls{wast}, \gls{sas} und \gls{bdst}.

\subsubsection{Web Application Security Testing (WAST)}
\label{sec:wast}
\gls{wast} definiert automatisierte Sicherheitstests, wo eine Webanwendung über die Benutzeroberfläche angegriffen wird.
Bei der Durchführung eines solchen Testverfahrens werden traditionell drei Schritte durchgeführt: \textit{spider scan}, \textit{active scan} und Berichterstellung. \autocite{rangnau_continuous_2020}

Ein \textit{spider scan} durchsucht die Anwendung nach allen verfügbaren Ressourcen.
Dabei wird der vom Webserver gelieferte Quellcode wie \gls{html} und \gls{js} Dateien nach \gls{url} Adressen durchsucht.
Die gefundenen Adressen werden erneut nach Ressourcen angefragt und alle Adressen gespeichert. \autocite{ikerionwu_focused_2020}

Der \textit{active scan} führt anschließend \textit{bösartige} Anfragen bzw. Angriffe gegen diese Ressourcen aus, sammelt und bewertet die erhaltenen Antworten vom Webserver.
Abschließend wird bei der Berichterstellung die evaluierten Antworten aggregiert und in einem Bericht zusammengeführt.

\subsubsection{Security API Scanning (SAS)}
\label{sec:sas}
\gls{sas}-Techniken testen \gls{api}s nach detaillierten Sicherheitsmechanismen wie Authentifizierung, Autorisierung, Eingabevalidierungen und Fehlerbehandlungen.
Im Vergleich zu \gls{wast}-Techniken wird hierbei keine Website nach weiteren Ressourcen gescannt sondern die jeweiligen Endpunkte einer \gls{api} direkt getestet. \autocite{rangnau_continuous_2020,hsu_practical_2019}

In \gls{sas} werden oftmals dieselben Endpunkte mehrfache mit verschiedenen Eingabewerten und angepassten Requestheadern angefragt, um Brute-Force-Angriffe auszuführen, welche Authentifizierungsmechanismen umgehen oder erfolgreiche \gls{sqli} durchführen können.
Hierfür können Fuzz Daten bzw. umfangreiche Listen als Eingabeparameter für die Anfragen verwendet werden \autocite{hsu_practical_2019}.
Bei dieser Technik übernimmt die \gls{sas}-Komponente das Erstellen und Abschicken von solchen Anfragen.
Damit die Antworten von den jeweiligen Requests evaluiert werden können und ein Bericht erstellt werden kann, wird hierbei ein Proxy zwischen der \gls{sas} Komponente und der zu testenden Zielanwendung geschaltet wie \autoref{fig:sas}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{image/aufbaukomponenten.PNG}
	\caption{Aufbau und Kommunikationswege der Hauptkomponenten eines SAS-Prozesses untereinander}
	\label{fig:sas}
\end{figure}

\subsubsection{Behaviour Driven Security Testing (BDST)}
\label{sec:bdst}
\gls{bdd} ist eine Erweiterung von \gls{tdd}, welches sich darauf konzentriert, Spezifikationen des Verhaltens vom Zielsystem so zu definieren, dass diese automatisiert werden können \autocite{solis_study_2011}.
Die Tests sind in einer spezifischen und allgegenwärtigen Sprache geschrieben, die insbesondere den Stakeholdern hilft, ihre Tests zu spezifizieren.
Dies soll dabei helfen, das Verhalten und erwartete Ergebnis von Testfällen einfach zu definieren.
\gls{bdst} ist eine Erweiterung von \gls{bdd} um den Sicherheitsaspekt.
\gls{ui}-Tests verwenden \gls{bdd}-Frameworks, um Standard-UI-Tests zu automatisieren, welche Benutzerverhalten nachahmen \autocite{lenka_behavior_2018}.
Dieser Ansatz wird in \gls{bdst} verwendet, um aus der Sicht eines Angreifers die entsprechenden Angriffsszenarien über die \gls{ui} auszuführen.

\section{Quality Assurance (QA)}
\label{sec:qa}
\gls{qa} bzw. Qualitätssicherung definiert den Bereich in der Softwareentwicklung, welcher dafür verantwortlich ist, die Qualität eines Softwareprodukts anhand acht Merkmale wie der Funktionalität, Zuverlässigkeit, Effizienz und Wartbarkeit sicherzustellen \autocite{iso_isoiec_nodate}.

Erich et al. haben gezeigt, dass dieser Aufgabenbereich wichtig ist, um die Entwicklung qualitative hochwertiger Software sicherzustellen \autocite{erich_qualitative_2017}.
Einige Studien haben sich auch mit der Bedeutung der \gls{qa} für die Softwareentwicklung in Bezug auf \gls{devops} auseinandergesetzt.
Perera et al. haben mittels verschiedener Regressionsanalysen ein Modell aufgestellt, welches die Berechnung der Softwarequalität anhand verschiedener Faktoren aus dem \gls{devops} Bereich ermöglicht \autocite{perera_improve_2017}.
Für dieses Modell wurden nach Humble et al. die vier \gls{cams} Indikatoren für die \gls{devops} Praktiken verwendet \autocite{humble_continuous_2010}.
Das Modell beweist, dass die verwendeten \gls{cams} Faktoren aus dem \gls{devops}-Bereich alle einen deutlichen Einfluss auf die Softwarequalität haben.
Insbesondere die Automatisierung bietet einen signifikanten Vorteil, um die Qualität der Software zu erhöhen.
\gls{qa} ist somit einer der Hauptbereiche, in denen \gls{devops} einen erheblichen Einfluss haben \autocite{toh_adoption_2019}.

Seit 2011 wurde der Standard von Qualitätsmerkmalen überarbeitet und beinhaltet ausdrücklich die Sicherheit als Merkmal \autocite{iso_isoiec_nodate}.
Somit ist der Bereich der \gls{qa} ebenfalls dafür verantwortlich die Schutzziele der IT-Sicherheit wie Vertraulichkeit, Integrität und Authentizität sicherzustellen.

Da die Sicherstellung der jeweiligen Qualitätsmerkmale eine umfangreiche Aufgabe je nach Geschäftsbereich und -umfang sein kann, können diese Bereiche in den Unternehmen weiter spezifiziert sein.
\textit{LichtBlick} besitzt einen eigenen \gls{qa} Bereich, welcher stetig ausgebaut und erweitert wird.
Der Bereich der IT-Sicherheit ist zum aktuellen Stand jedoch noch von der \gls{qa} gelöst und agiert eigenständig bzw. unabhängig von den durchgeführten \gls{qa} Prozessen.
Im weiteren Verlauf der Arbeit werden beide Bereiche unter Sec/QA definiert. 