\chapter{Einführung in das Thema}
\label{chap:einleitung}
In den letzten Jahren hat die Digitalisierung sowie Automatisierung von Geschäftsprozessen stark zugenommen \autocite{shirer_new_2021}.
Insbesondere der Ausbau sowie finanzielle Ausgaben von Cloudinfrastrukturen steigen jährlich weiterhin an \autocite{shirer_cloud_2022}.
Cloudanbieter wie Amazon, Google und Microsoft bieten weltweit \textit{Products-as-a-Service} an, was die Verwendung von Software sowie ganze IT-Infrastrukturen vereinfacht \autocite{abdalla_advantages_2019}. 
Durch die stetig steigende Nachfrage an Softwarelösungen nimmt auch das Angebot von IT-Dienstleistern zu \autocite{pettey_gartner_2022}.
Anhand des hohen Interesses an qualitativer und auf das Geschäftsmodell zugeschnittener Software wollen auch die IT-Dienstleister schnellstmöglich ihre Produkte entwickeln und verkaufen.

Die Entwicklung von Software folgt traditionell dem \gls{sdlc}.
Dieser beschreibt die einzelnen Phasen, welche eine Software im Laufe der Entwicklung durchläuft.
Dabei werden folgende sechs Phasen durchlaufen: Analyse, Design, Entwicklung, Testen, Bereitstellung und Wartung.
Der \gls{sdlc} kann sowohl als Wasserfall, V- oder agiles Modell konzipiert werden. \autocite{balaji_wateerfallvs_2012}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\linewidth]{image/sdlc2.PNG}
	\caption{Phasen-Modell des SDLC}
	\label{fig:sdlc}
\end{figure}

Die Anforderungen von Kunden können sich im Laufe der Produktentwicklung ändern.
Außerdem kann die Entwicklung von anspruchsvoller Software mehrere Jahre in Anspruch nehmen.
Daher empfiehlt es sich häufig, iterative bzw. agile Prozesse zu verwenden \autocite{stadler_agile_2019}.
Damit der Kunde in kurzen Zeitintervallen Auszüge seines Produkts und der IT-Dienstleister dadurch kontinuierliches Feedback vom Kunden erhält, müssen die Phasen des \gls{sdlc} für einzelne \textit{Features} statt der gesamten Software durchlaufen werden.
Wie \autoref{fig:sdlc} zeigt, wird nach Abschluss der letzten Phase der \gls{sdlc} erneut angestoßen.
Durch diesen kontinuierlichen Kreislauf der Softwareentwicklung eines Produkts werden regelmäßige \textit{Releases} einer Software durchgeführt.
Da jede neue Codeänderung Fehler und Schwachstellen beinhalten kann, stehen die IT-Dienstleister in der Verantwortung, dass die einzelnen Softwarepakete an den Kunden fehlerfrei und getestet bereitgestellt werden.

Dieser Prozess der kontinuierlichen Bereitstellung lässt sich automatisieren und ist als \gls{cicd} definiert \autocite{microsoft_continuous_2022}.
Insbesondere der Term \gls{devops} bezeichnet die Entwicklung des Produkts sowie die Aufgabe diese automatisierten Prozesse zur kontinuierlichen Integration und Bereitstellung zu implementieren \autocite{bass_devops_2015}.

\section{Problemstellung und Unternehmensbezug}
\label{sec:problemstellung}

Durch den beschleunigten Zuwachs von steigenden komplexeren Anforderungen und neuen Technologien sind IT-Dienstleister nicht nur mit der Entwicklung und Bereitstellung von Software beschäftigt sondern werden insbesondere immer mehr mit dem Thema IT-Sicherheit konfrontiert.
\autoref{fig:cyberdmg} zeigt, dass von 2011 bis 2021 die Anzahl bzw. die weltweit verursachten Schäden aufgrund von Cyberangriffen exponentiell gestiegen sind \autocite{abbate_federal_2021}.
Die jährlich entstehenden Schäden fallen in Milliarden US-Dollar aus.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{image/cyberdmg.PNG}
	\caption{Höhe des weltweiten monetären Schadens von Cyberattacken in Millionen US-Dollar, der von 2001 bis 2021 zugefügt wurde \autocite{abbate_federal_2021,statista_cyber_2022} }
	\label{fig:cyberdmg}
\end{figure}

Dies zeigt die Notwendigkeit, die eigene Software nach Sicherheitslücken zu überprüfen.
Diese Überprüfung der Softwaresicherheit lässt sich ebenfalls zum Teil automatisieren.
Durch die Einbindung von expliziten Sicherheitstests in den \gls{cicd}-Zyklus wurde der Term \gls{devsecops} definiert. \autocite{sanchez-gordon_security_2020} 

In den letzten Jahren wurden immer weitere Forschungsarbeiten zu diesem Thema geschrieben, da die Entwicklung und Forschung in diesem Themenbereich noch viel Potenzial bietet \autocite{rangnau_continuous_2020,sonmez_holistic_2021,mateo_tudela_combining_2020}.
Insbesondere stellt die Auswahl, Verwendung sowie Integration von passenden Tools eine Herausforderungen für die meisten Unternehmen dar \autocite{tomas_empirical_2019}.

Das Unternehmen \textit{LichtBlick} ist einer der größten Ökostromanbieter Deutschlands.
Mit Hauptsitz in Hamburg und Gründung im Dezember 1998 hat sich das Unternehmen in den vergangenen 23 Jahren einen Stamm von über 1.7 Millionen Kunden aufgebaut \autocite{lichtblick_pressestimmen_2023}.
Fast alle Geschäftsprozesse werden über die unternehmensinterne IT von \textit{LichtBlick} entwickelt und betreut.
Diese beinhaltet neben komplexen Kundenabrechnungssystemen auch die Entwicklung und Betreuung einer eigenen Website, welches als Self-Service-Portal fungiert.
Auf dieser Website können Kunden über ihr persönliches Kundenkonto diverse Informationen über ihren Stromverbrauch sowie ihre Abschlagsinformationen erhalten.
Außerdem können sie diese auch anpassen und ihren Vertrag online kündigen. 
Die Website ist an eine \gls{api} angebunden, welche personenbezogene und sensible Daten ausliefern kann.
Diese Daten gilt es im Sinne der Datenschutz-Grundverordnung besonders zu schützen \autocite{intersoft-consulting_erwagungsgrund_nodate}.

Die Vertraulichkeit, Integrität und Verfügbarkeit sind die wichtigsten Prinzipien, um IT-Sicherheit zu gewährleisten \autocite{perrin_cia_2008}. 
Entsprechende Sicherheitsmechanismen müssen für die Website und der \gls{api} implementiert werden, um diese vor Cyberangriffen zu schützen.  

\section{Betriebliche Voranalysen und Lösungsansatz}
\label{sec:lösungsansätze}
Der Self-Service wird von den zwei Teams \textit{Web-Frontend} und \textit{Web-Backend} entwickelt und betreut.
In beiden Teams existieren \gls{cicd}-Pipelines.
Dort werden verschiedene Prozesse durchlaufen, welche unter anderem den Code kompilieren, testen und auf einer Test- oder Produktivumgebung bereitstellen.
\gls{qa}-Beauftrage unterstützen diesen Prozess und sorgen auf den isolierten Testumgebungen mit umfangreichen Testverfahren und statischen Codeanalysen für die Einhaltung verschiedener Qualitätsmerkmale des Softwareprodukts. 
Außerdem werden die Features von den Entwicklern in isolierten bzw. unabhängigen eigene Git-Branches entwickelt.
Die Zusammenführung der angepassten und neuen Codestände in eine gemeinsame Hauptversion erfolgt über Peer-Reviews mittels Merge- bzw. Pull-Requests.
Diese Prozesse sorgen bei \textit{LichtBlick} bereits für qualitativ hochwertige Softwareprodukte.

Die Dienste bei \textit{LichtBlick} werden über einzelne Containerinstanzen in einem Kubernetes-Cluster in der Microsoft Azure Cloud bereitgestellt.
Container sind leichtgewichtige Virtualisierungsumgebungen, welche alle benötigten Abhängigkeiten und Dienste für eine Anwendung in einer einzigen Instanz bereitstellen und diese Anwendung ausführen können.
Ein Kubernetes-Cluster ist eine Gruppe von Knoten, die containerisierte Anwendungen ausführen.
Somit ermöglichen Kubernetes-Cluster eine einfachere Entwicklung, Verschiebung und Verwaltung von Anwendungen. \autocite{vmware_what_nodate}

Diese Dienste werden mit Rate Limiting Strategien vor automatisierten Bot-Angriffen weitestgehend abgesichert.
Weitere Mechanismen wie Healthchecks, komplexe Authentifizierungs- und Autorisierungsverfahren sorgen dafür, dass die drei Prinzipien der IT-Sicherheit gewährleistet sind.
Außerdem sorgen jährliche manuelle Penetrations- bzw. Pentests von externen Dienstleistern dafür, dass Schwachstellen in der Website erkannt und anschließend von der \textit{LichtBlick}-IT behoben werden.

Auch wenn somit eine Grundsicherheit besteht, werden keine kontinuierlicheren Pentests durchgeführt.
Aktuell werden alle zwei Wochen eine neue Version der Website mit neuen Features und Änderungen veröffentlicht.
Jede neue Version einer Software kann auch neue Schwachstellen beinhalten.
Um die Sicherheit zu erhöhen, sollten spätestens nach jedem \textit{Release} Pentests durchgeführt werden.

\textit{LichtBlick} lässt die Penetrationstests ausschließlich von externen Mitarbeitern ausführen.
Der manuelle Aufwand hinter solchen Tests kann hoch und zeitaufwändig sein.
Außerdem würden regelmäßige Pentests dem Unternehmen hohe Kosten verursachen.

Grundlegende und einfache Pentests können auch automatisiert ausgeführt werden.
Eine mögliche Lösung, um regelmäßige Penetrationstests auszuführen, ohne hohe Kosten zu verursachen und die IT-Sicherheit zu erhöhen, wären automatisierte Sicherheitstests in die \gls{cicd}-Pipelines mit aufzunehmen. 

Mit dieser Arbeit sollen Grundlagen zur Integration von \gls{dast}-Tools in bestehende \gls{cicd}-Pipelines geschaffen werden.
Außerdem soll durch ein Prototyp bzw. einer Fallstudie nicht nur praktische Erfahrung in diesem Bereich gesammelt sondern vor allem Herausforderungen und Probleme bei der Integration ermittelt werden.
Diese Beobachtungen und Ergebnisse der Fallstudie sollen kritisch diskutiert und Lösungsansätze geschaffen werden, damit dieses Wissen dabei unterstützt mit geringem Aufwand im Unternehmen dynamische Sicherheitstests in \gls{cicd}-Pipelines einzuführen.

\section{Forschungsfragen und Aufbau der Arbeit}
\label{sec:aufbauderarbeit}
In dieser Arbeit wird anhand einer Fallstudie die Herausforderungen der Integration von dynamischen Sicherheitstests in \gls{cicd}-Pipelines ermittelt.

Es stellen sich folgende Forschungsfragen:
\begin{itemize}
    \item[\textbf{F1}] Wie werden \gls{cicd}-Prozesse implementiert und wie können dynamische Sicherheitstests dort integriert werden?
    \item[\textbf{F2}] Welche \gls{dast}-Techniken gibt es und wie funktionieren diese konkret?
    \item[\textbf{F3}] Welche \gls{devsecops}-Anforderungen gibt es, die eine \gls{cicd}-Pipeline mit Sicherheitstests bestehen muss?
    \item[\textbf{F4}] Wie effizient sind die jeweiligen \gls{dast}-Techniken im Vergleich zueinander? 
    \item[\textbf{F5}] Welche Herausforderungen und Probleme sind bei der Integration von \gls{dast}-Tools in bestehende \gls{cicd}-Pipelines zu erwarten und wie lassen sich diese beheben?
\end{itemize}

Anhand diesen fünf Forschungsfragen wird die Arbeit aufgebaut.
\autoref{chap:grundlagen} schafft diverse Grundlagen, welche benötigt werden, um die Thematik von Virtualisierungstechniken, den \gls{cicd}-Prozess sowie verschiedene Arten von Sicherheitstests im Detail verstehen zu können.
Anschließend werden die für die Fallstudie ausgewählten Tools in \autoref{sec:dasttools} vorgestellt.
Forschungsarbeiten, welche einen besonderen Einfluss auf diese Arbeit haben, werden in \autoref{chap:relatedWork} zusammengefasst.
\autoref{chap:experiment} stellt die Fallstudie im Detail dar, in dem das Lösungskonzept bzw. der Aufbau des Experiments, sowie die Durchführung und Beobachtungen beschrieben werden.
Im anschließenden \autoref{chap:diskussion} werden die Beobachtungen der Fallstudie kritisch bewertet indem die Herausforderungen zusammengefasst und mögliche Lösungsansätze hierfür herausgearbeitet werden.
Im letzten \autoref{chap:fazitundausblick} wird die Arbeit zusammengefasst und mögliche Ausblicke zu weiteren Forschungsansätze gegeben, welche mit dieser Arbeit möglich geworden sind.
