\chapter{Diskussion}
\label{chap:diskussion}
In diesem Kapitel wird die letzte Forschungsfrage \textbf{F5} beantwortet, indem die Beobachtungen sowie Bewertungen aus dem Experiment gruppiert und zusammengetragen werden.
Anschließend werden die Ergebnisse kritisch diskutiert und Lösungsansätze zu den Herausforderungen dargestellt.

\section{Herausforderungen}
\label{sec:herausforderungen}
Die Herausforderungen lassen sich nach den beiden Zuständigkeitsgruppen \gls{devops} und \gls{qa} bzw. Security klassifizieren.

\subsection{Bereich DevOps}
\label{subsec:bereichdevops}
\autoref{tab:devopsherausforderungen} zeigt die Herausforderungen der \gls{devops} Zuständigkeitsgruppe.
Die jeweiligen Herausforderungen haben Kennzahlen zur Komplexität $K$ und Relevanz $R$.
Dabei definiert $K$ die zu erwartende Komplexität zur Lösung der Herausforderung und $R$ die zugehörige Relevanz diese Herausforderung tatsächlich zu lösen.
Für jedes $x$ aus $K$ oder $R$ gilt $x \in \{1,2,3\}$.
Diese Ziffern sind Synonyme für niedrig, mittel und hoch.
Z.B. steht die Ziffer 1 in $x$ äquivalent für eine niedrige Komplexität oder Relevanz zur Problemlösung der Herausforderung.
Die Bestimmung der jeweiligen Kennzahlen sind Schätzungen, welche in jeder Herausforderung begründet werden.
Diese Kennzahlen werden im späteren Verlauf verwendet, um eine geordnete Liste der zu erwartenden Herausforderungen bei der Integration von \gls{dast} in \gls{cicd}-Pipelines zu erstellen.


\begin{table}[H]
    \centering
    \begin{tabularx}{\textwidth}{|l|X|r|r|}
    \hline
    \textbf{Bezeichner} & \textbf{Beschreibung}                             & \textbf{Komplexität} & \textbf{Priorität} \\ \hline
    H1                  & Implementierung und Bereitstellung eigener Runner & 2                    & 3                 \\ \hline
    H2                  & Caching von Images                                & 3                    & 3                 \\ \hline
    H3                  & Extra Build Phase                                 & 1                    & 2                 \\ \hline
    H4                  & BDST                                              & 3                    & 1                 \\ \hline
    H5                  & WAST                                              & 1                    & 1                 \\ \hline
    \end{tabularx}
    \caption{DevOps Herausforderungen}
    \label{tab:devopsherausforderungen}
\end{table}

Wie \autoref{tab:devopsherausforderungen} zeigt, haben sich fünf Herausforderungen für den \gls{devops}-Bereich ermitteln lassen.
Diese werden im Folgenden vorgestellt und die möglichen Lösungsansätze diskutiert. 

\subsubsection{H1 - Eigene Runner}
Eigene Runner sind für öffentliche bzw. kleine Projekte, welche keine nennenswert schützenden Daten verarbeiten, kein Kriterium für die \gls{dast} Integration in eine \gls{cicd}-Pipeline.
Somit würden öffentliche \textit{GitLab Runner} verwendet werden, welche kompletten Zugriff auf den Quellcode des Repositorys haben.
Da ein öffentlicher \textit{GitLab Runner} von jeder Person bereitgestellt werden kann, könnten Geheimnisse von privaten Repositories gestohlen werden.
Außerdem werden durch die Verwendung von öffentlichen \textit{GitLab Runnern} alle Kontrollmechanismen an den Verwalter des jeweiligen \textit{Runners} abgegeben.
Dadurch können Verzögerungen auftreten und keine eigenen Caching-Strategien auf dem Server implementiert werden. 
Außerdem können die Leistungen sowie tatsächlich durchgeführten Arbeitsschritte des Servers nicht eingesehen werden. 

Die Repositories des Unternehmens \textit{LichtBlick} sind privat und beinhalten somit Quellcode, welcher nicht öffentlich eingesehen werden soll.
Insbesondere stellt der Quellcode des Unternehmens ein Geschäftsgeheimnis dar, welches es zu schützen gilt.
Somit ist die Implementierung eines eigenen \textit{Runners} eine Grundvoraussetzung zur \gls{dast} Integration.
Durch die genannten Vorteile der vollständigen Kontrolle der Konfigurationen und Hardware des Servers und Dienstes, können somit auch diverse Monitoraufgaben implementiert werden.

Aufgrund umfangreicher Dokumentationen zur Implementierung und Bereitstellung eigener \textit{GitLab Runner} ist die Komplexität zur Umsetzung der Herausforderung im mittleren Bereich eingestuft, da neben der Beschaffung eines geeigneten Servers auch der Umfang an Konfigurationsmöglichkeiten eines \textit{GitLab Runners} trotz Dokumentationen nicht trivial ist bzw. eine gewisse Einarbeitungszeit voraussetzt.  

\subsubsection{H2 - Caching von Images}
Diese Herausforderung ist aus der Anforderung \textbf{A4} entstanden, welche in dem Projekt nicht erfüllt werden konnte. 
Bei jedem Durchlauf einer \gls{cicd}-Pipeline werden alle Images neu gebaut.
Dabei wird nicht berücksichtigt, ob es Quellcodeanpassungen in den jeweiligen Images gab.
Durch diesen Prozess werden nicht nur die Ressourcen des \textit{GitLab Runners} beansprucht, sondern auch die Gesamtdauer eines Builds.
Da Entwickler, \gls{qa}-Beauftragte und weitere Prozesse auf das Feedback des Pipelinestatus angewiesen sind, ist es von hoher Bedeutung, das die Pipeline schnell läuft.
Somit ist die Anforderung, dass nur benötigte und veränderte Komponenten während der Pipeline gebaut werden, ein wichtiges Kriterium zur \gls{dast} Integration, da bei diesem Prozess die genannten Ressourcen freigeräumt werden können.

Um dies zu erreichen, müssen Caching-Strategien der Images implementiert werden.
Außerdem muss ein Kontrollmechanismus bereitgestellt werden, welcher verifizieren kann, ob sich Quellcode im Vergleich zum letzten Stand verändert hat.
Das Caching von Docker Images kann über den Docker Daemon realisiert werden.
Da der Prozess der dynamischen Sicherheitstests in der eigene \textit{.gitlab-ci.yml} als \textit{Docker-in-Docker} Prozess definiert ist, wird bei jedem Job ein eigener Docker Daemon initialisiert, welcher normalerweise keine zwischengespeicherten Images oder Container bereitstellt.

Eine Möglichkeit, um an diesen Stellen an zwischengespeicherten Images zu gelangen, ist die Verwendung eines globalen bzw. gemeinsamen Docker Daemons wie \autoref{fig:caching2} zeigt. 
Die Voraussetzung ist die Verwendung eines eigenen \textit{Runners}.
Der Server auf dem dieser \textit{Runner} installiert ist, benötigt den Docker Dienst und einen eigenen Docker Daemon. 
Dieser Daemon kann mit dem Start eines jeden Runners an diesen gebunden werden, sodass jeder isolierte Job auf den Docker Daemon des Hostsystems zugreifen kann.
Dadurch würde nach einmaligem Abruf eines Images aus einem Repository dieses im Daemon gespeichert werden.
Zukünftige Jobs sparen sich somit die Zeit des Herunterladens von Images, welche bereits vorhanden sind.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{image/cachingstrategie3.PNG}
	\caption{Struktur und Aufbau der benötigten Komponenten für eine Caching-Strategie}
	\label{fig:caching2}
\end{figure}

Docker verwendet ein intelligentes Layer- bzw. Schichtenspeichersystem, welches den benötigten Kontrollmechanismus bietet, um zu erkennen, ob ein Images geändert wurde oder nicht.
Images werden über Dockerbefehle wie \textit{RUN, COPY, ADD} etc. in einem \textit{Dockerfile} definiert.
Jedes dieser Befehle speichert den Aufruf in einem eigenem Layer im Docker Daemon ab.
Durch den Bau eines Images werden alle Schichten des aktuellen Images mit dem des zwischengespeicherten Image aus dem Docker Daemon verglichen.
Bei einer Abweichung eines Layers wird ausschließlich dieses Layer neu erstellt.
Somit können alle anderen Schichten weiterhin verwendet werden, ohne erneut gebaut werden zu müssen.

Auch wenn die Einbindung eines gemeinsamen Docker Daemons für jeden Job als eine mögliche Lösung verwendet werden kann, beinhaltet dies Sicherheitsrisiken.
Die Isolierung der Jobs untereinander hat den Vorteil, dass beim \gls{cicd} Prozess versehentlich keine falschen Codestände verwendet werden können. 
Ein Docker Image wird zur Unterscheidung von verschiedener Versionen mit \textit{tags} markiert. 
Da die Jobs parallel und unabhängig voneinander auf den selben Docker Daemon zugreifen würden und dort ihre Images verwalten, ist eine genaue und korrekte Versionskontrolle der Images nötig. 
Durch fehlerhafte Konfigurationen dieser \textit{tags} können Versionskonflikte und inkorrekte Versionen getestet oder sogar produktiv ausgeliefert werden.
Bei nachlässigem Monitoring kann dies erst bei Fehlern im Produktivsystem auffallen und zu Schäden führen.
Des Weiteren kann durch das Zwischenspeichern von diversen Images und Containern den Serverspeicher vollständig belegen, sodass keine neueren Images gespeichert oder sogar Prozesse gestartet werden können.
Es gilt somit durch weitere Prozesse unnötig belegte Ressourcen freizuräumen.

Die Umsetzung dieser Herausforderung setzt die Fertigstellung von \textbf{H1} voraus. 
Aufgrund der geschilderten Situationen und entstehenden Mehraufwände müssen weitere Analysen in diesem Bereich betrieben werden.
Aus diesem Grund ist die Umsetzung dieses Problems mit einer hohen Komplexität versehen.   

\subsubsection{H3 - Extra Build Phase}
Im Vergleich zur Referenzarbeit von Rangnau et al. wurden nur die beiden Phasen \textit{test} und \textit{evaluate} für die Pipeline definiert.
Der Ansatz war hierbei die Komplexität zu reduzieren und Bauzeiten einzusparen.
Durch die Integration einer \textit{build} Phase, welche ausschließlich für das Erzeugen der Images zuständig ist, können folgende Vorteile entstehen:
\begin{itemize}
  \item Klarere Aufgabentrennung der Jobs nach Zuständigkeit
  \item Nachvollziehbarkeit des Statuscodes
  \item Redundante Images einmalig bauen 
\end{itemize}

Durch eine klare Abgrenzung der Zuständigkeiten von Jobs wird das Single-Responsibility-Prinzip eingehalten.
Dies ist einer der fünf SOLID Prinzipien, welche dabei unterstützen sollen, wartbarere Software zu implementieren.
Der Statuscode eines Jobs kann somit besser nachvollzogen werden.
Ein fehlgeschlagener Job sollte eindeutig und ohne Verwendung der Logs interpretiert werden können.
Abonnenten einer Pipeline wie Entwickler und \gls{qa}-Beauftragte werden über E-Mail benachrichtigt, wenn die Pipeline fehlschlägt. 
In dieser Nachricht sollte erkennbar sein, welche Jobs fehlgeschlagen sind.
Da verschiedene Zuständigkeitsgruppen den Status der Pipeline abonnieren, ist es zeitsparend, wenn die Verantwortlichkeit des Fehlers an die richtige Zuständigkeitsgruppe direkt weitergeleitet wird.
Z.B. könnte die Verantwortlichkeit von Tests ausschließlich dem \gls{qa}-Bereich obliegen, während Jobs der \textit{build} Phase nur für Entwickler und \gls{devops} Verantwortliche relevant sind.
In der Fallstudie hat sich gezeigt, dass durch das Einsparen einer \textit{build} Phase der Statuscode eines \textit{test} Jobs mehrdeutig sein kann.
Es ist anhand eines fehlgeschlagenen \textit{test} Jobs somit nicht ersichtlich, ob der Fehler beim Testen oder beim Bau eines Images aufgetreten ist, da die beiden Schritte in dieser Phase durchgeführt werden.
Die Einführung einer separaten \textit{build} Phase kann eine mögliche Lösung zu diesem Problem sein.

Es hat sich gezeigt, dass Images wie \textit{owasp/zap2docker-stable} oder die Webanwendung \textit{bkimminich/juice-shop} mehrfach für verschiedenen Jobs verwendet werden.
\textit{GitLab CI} bietet die Möglichkeit Images in einer Container-Registry abzuspeichern.
In einer \textit{build} Phase können somit die Images in dieser Container-Registry abgespeichert werden.
Andere Jobs können sich anschließend an diesem Speicher bedienen, um die einmalig gebauten Images herunterzuladen und zu verwenden. 
Die Verwendung dieser Container-Registry wäre ebenfalls eine Möglichkeit, um die Herausforderung \textbf{H2} zu lösen.
Jedoch befindet sich dieser Speicher nicht auf dem selbem Server wie der Dienst, wo der \textit{GitLab Runner} installiert ist.
Somit würden weiterhin für jeden Job die Downloadzeiten anfallen.
Außerdem werden die Jobs parallel ausgeführt.
Da jeder Job der \textit{test} Phase auf die Fertigstellung aller Jobs der \textit{build} warten und dennoch alle Images aus einem externen Speicher heruntergeladen werden müssen, lohnt sich die Verwendung einer Container-Registry nicht für diese Fallstudie.

Insgesamt besteht die Notwendigkeit einer \textit{build} Phase zur Optimierungen des Arbeitsprozesses und direkte Zuordnung der Zuständigkeiten bei fehlerhaften Jobs.
Dies ist mit einer mittleren Priorität einzuordnen, während die Komplexität zur Umsetzung als niedrig definiert wurde.  

\subsubsection{H4 - BDST}
Bei der Verwendung von \gls{bdst}-Techniken mittels Selenium wurden folgende Probleme beobachtet:
\begin{itemize}
  \item Wiederholung von Jobs
  \item Hoher Ressourcenverbrauch
\end{itemize} 

Beim Testen mittels \gls{bdst} wird für jeden Testlauf ein neuer Browser im Container gestartet.
Die Testfälle in Selenium sind so definiert, dass jede Benutzerinteraktion im Browser in Echtzeit durchgeführt wird.
Manche Benutzeraktionen können erst nach der Durchführung konkreter Arbeitsschritte ausgeführt werden.
Hierbei kann es dazu kommen, dass die Testausführung schneller arbeitet als das die einzelnen \gls{html}-Elemente in den Browser korrekt geladen werden.
Auch wenn an manchen Stellen explizit gewartet wird, können solche Situationen stets auftreten und somit Testfälle nicht korrekt durchführen.
Der Container wird jedoch nicht mit einem entsprechenden Exitcode beendet, weshalb der Job trotz nicht korrekt durchgeführter Testfälle als bestanden deklariert wird.
Um die vollständige Testdurchführung zu verifizieren, müssen weitere Mechanismen implementiert oder manueller Aufwand betrieben werden.
Das Risiko von inkorrekt durchgeführten Tests und somit der Wiederholung von Jobs wächst linear mit der Anzahl von zunehmenden Testfällen.

Die Durchführung von \gls{bdst} verbraucht aufgrund der tatsächlichen Durchführung von Benutzerinteraktionen über einen Browser am meisten Ressourcen.
Die Ressourcen sind hierbei nicht nur der verwendete Speicher sondern insbesondere die beanspruchte Zeit des Prozessors.
Jeder Testlauf startet einen neuen Browser und die meisten Anweisungen der Testfälle haben eine festgesetzte Wartezeit bevor die nächste Aktion ausgeführt wird.

Die genannten Herausforderungen zu lösen, ist eine Aufgabe von hoher Komplexität.
Eine Möglichkeit, um die Testausführung zu gewährleisten, ist das Implementieren von Wartezeiten.
Diese können sowohl konstante als auch dynamische Werte sein, die entweder eine definierte Zeitspanne oder auf das Laden von konkreten Elementen warten.
Durch diesen Ansatz benötigt die Testausführungszeit jedoch deutlich länger und kann nicht vollständig die korrekte Ausführung sicherstellen.
Deshalb eignet sich \gls{bdst} nicht, um automatisiert in einer \gls{cicd}-Pipeline ausgeführt zu werden. 
Dieses Problem zu lösen, hat eine niedrige Relevanz, da bereits festgestellt wurde, das \gls{bdst} sich nicht für \gls{dast} in \gls{cicd}-Pipelines eignet.

\subsubsection{H5 - WAST}
Aufgrund der bereits integrierten \textit{Scanning-Scripts} im \textit{owasp/zap2docker-stable} Images, ist die \gls{wast} Integration mit wenig Aufwand durchzuführen.
Jedoch wurde beobachtet, dass der verwendete Basisscan nur eine Minute braucht und dabei nichts nennenswertes feststellen kann.
Ein vollständiger Scan kann je nach Umfang der zu testenden Webanwendung bis zu mehrere Stunden benötigen.
Aus diesem Grund ist \gls{wast} generell ineffizient bzw. ungeeignet, um nennenswerte Resultate in einer \gls{cicd}-Pipeline in angemessener Zeit zu erzielen.

Eine mögliche Lösung, um \gls{wast}-Techniken zu integrieren, besteht darin, die Zeit für den Basisscan zu erhöhen.
Da dies über einen Parameter dem verwendeten Skript mitgegeben werden kann, ist die Komplexität zur Lösung niedrig zu bewerten.
Wie bereits erkannt, würden dadurch die Testdauer erhöht werden und nennenswerte Resultat höchstwahrscheinlich weiterhin ausstehen.
Aus diesem Grund ist die Relevanz zur Problemlösung als niedrig bewertet worden.


\subsection{Bereich Sec/QA}
\label{subsec:bereichsecsq}
In diesem Abschnitt werden die drei ermittelten Herausforderungen des Sec/\gls{qa}-Bereichs vorgestellt, welche in \autoref{tab:qaherausforderungen} dargestellt sind.

\begin{table}[H]
  \centering
  \begin{tabular}{|l|l|r|r|}
  \hline
  \textbf{Bezeichner} & \textbf{Beschreibung} & \textbf{Komplexität} & \textbf{Priorität} \\ \hline
  H6                  & Umgang der ZAP-Berichte          & 2                    & 3                 \\ \hline
  H7                  & Testerstellung        & 3                    & 3                 \\ \hline
  H8                 & Berichtsformate       & 2                    & 2                 \\ \hline
  \end{tabular}
  \caption{Sec/QA Herausforderungen}
  \label{tab:qaherausforderungen}
\end{table}

\subsubsection{H6 - Umgang der ZAP-Berichte}
Es konnte beobachtet werden, dass die erstellten Berichte von \gls{zap} nicht immer eindeutig sind.
Beim Juice Shop wird beim Versuch eine Webadresse zu erreichen, welche es nicht gibt, eine Umleitung auf die Startseite ausgeführt.
Bei der \gls{wast} Durchführung konnte festgestellt werden, dass \textit{http://juice-shop:3000/sitemap.xml} dieselben Ergebnisse liefert wie \textit{http://juice-shop:3000}.
Erst nach manueller Replikation des Testfalls, konnte festgestellt werden, dass die gefundenen Schwachstellen nicht der \gls{url} Adresse \textit{/sitemap.xml} sondern die der Startseite sind.
Es lässt sich daraus ableiten, dass jede Instanz einer berichteten Schwachstellen im \gls{zap} Bericht manuell nachgeprüft werden muss.
Eine mögliche Lösung, damit solche Vorfälle wie das Folgen von \textit{Redirects} zukünftig nicht mehr geschehen, wären Anpassungen der Konfigurationsdaten des Proxys.  

Die Experten, welche die Berichte analysieren und entsprechende Maßnahmen ergreifen, benötigen ein umfangreiches Fachwissen im Software Sicherheitsbereich.
Zwar werden zu jedem Schwachstellenbericht vorgefertigte Beschreibungs- und Lösungsansätze inklusiver Referenzlinks angezeigt, dennoch muss jeder Fall für sich genaustens geprüft und analysiert werden.
Es wird daher empfohlen, die Berichte an \gls{qa} Experten und Entwicklern zu leiten, welche eine umfangreiche Schulung in den Sicherheitsbereichen absolviert haben.   

Aufgrund der benötigten Schulungen von Mitarbeitern und der Aufwand von manueller Nacharbeit der gefundenen Schwachstellen, wird die Komplexität zur Lösung des Problems im mittleren Bereich eingeordnet.
Da eine korrekte und umfangreiche Behandlung der gefundenen Schwachstellen essentiell und wesentlicher Grund zur \gls{dast} Integration in \gls{cicd}-Pipelines ist, wird die Relevanz hoch eingestuft.

\subsubsection{H7 - Testerstellung}
Ähnlich wie bei Herausforderung \textbf{H6} sollten im Sicherheitsbereich geschulte Entwickler oder \gls{qa}-Beauftragte die Testfälle erstellen.
Wie die Fallstudie gezeigt hat, können viele Testfälle über Live-Benutzerinteraktionen mit einem Browser aufgenommen und als Testdatei abgespeichert werden.
Damit diese Testdatei auch in einer \gls{cicd}-Pipeline erfolgreich läuft, müssen manuelle Anpassungen wie der Überreichung der zu testenden Webadresse und des zwischengeschalteten Proxys durchgeführt werden. 

Nach Rangnau et al. sei das Zusammenspiel mehrere Testtechniken sinnvoll, um umfangreiche Ergebnisse zu den Schwachstellen zu erzielen \autocite{rangnau_continuous_2020}.
Anhand der beobachteten Werte bzgl. der Effizienz der Testtechniken zueinander, empfiehlt es sich, \gls{sas} Testtechniken für automatisiert dynamische Sicherheitstests in \gls{cicd}-Pipelines zu implementieren.
Ein mögliches Konzept zur optimalen Testerstellung wäre somit die Integration der relevanten \textit{Web Requests} von \gls{wast} und \gls{bdst} in \gls{sas} Szenarien.

Die Erstellung von qualitativ hochwertigen Testfällen ist einer der wichtigsten Aufgaben beim automatisierten Testen, weshalb es mit einer hohen Priorität bewertet wurde.
Des Weiteren ist es eine komplexe Aufgabe, da diverse Techniken des Testens nicht nur beherrscht werden müssen sondern auch das nötige Wissen benötigt wird, um aus der Sicht eines Angreifers entsprechende Schwachstellen zu finden.

\subsubsection{H8 - Berichtsformate}
In der Fallstudie ist aufgefallen, dass die \gls{zap} Berichte in den beiden Formaten \gls{html} und \gls{pdf} generiert wurden.
Außerdem werden die Berichte generell nach ihrer Testtechnik erstellt.
Für die Berichtsanalyse kann es wünschenswert sein, einen einzigen Bericht mit allen gefundenen Schwachstellen zu besitzen.
Aus diesem Grund sollte eine Lösung implementiert werden, wodurch es einen einheitlichen Sicherheitsbericht für alle Testtechniken gibt.

Die \gls{api} von \gls{zap}, welche bereits zur Generierung der \gls{pdf} Berichte verwendet wird, stellt auch die Möglichkeit zur Verfügung, komprimierte Berichte im \gls{json} Format zu erstellen.
Diese Variante kann für alle \gls{dast}-Techniken verwendet werden und somit die \gls{html} und \gls{pdf} Formate ersetzen.
Nach der Evaluierungsphase kann in der Pipeline ein finaler Job erstellt werden, welcher den Inhalt der drei \gls{json} Dateien zusammenfasst und ggf. mithilfe von der \gls{zap} \gls{api} einen einzigen und übersichtlichen Bericht im \gls{pdf} oder \gls{html} Format erstellt.
Außerdem bieten diese \gls{json}-Daten eine bessere Möglichkeit die Ergebnisse in weiteren Prozesse zu verarbeiten wie in der Evaluierungsphase oder bei der Steuerung von Notifikationen. 

Der Aufwand zur Erstellung des beschriebenen Prozesses sowie die Priorität dieses Problem zu lösen, werden auf einen mittleren Wert geschätzt.
Sofern sich dazu entschieden wird, nur eine \gls{dast} Technik zu automatisieren, entfällt diese Herausforderung. 

\subsection{Zusammenfassung}
\label{subsec:zusammenfassung}
Durch die Einordnung der Herausforderungen nach den beiden Kennzahlen Priorität und Komplexität lässt sich die Matrix aus \autoref{fig:herausforderungen2} aufstellen.
Anhand dieser Matrix lässt sich erkennen, welche Maßnahmen mit einer hohen Priorität versehen sind und welche Komplexität bei der Lösung zu erwarten ist.
Es lässt sich somit ablesen, dass die vier Maßnahmen \textbf{H1}, \textbf{H2}, \textbf{H6} und \textbf{H7} aufgrund der hohen Priorität als erstes gelöst werden sollten.
Dies beinhaltet die Implementierung eigener \textit{Runner}, Caching von Images sowie den Umgang mit den \gls{zap} Berichten als auch die Erstellung von qualitativen Sicherheitstests.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.65\linewidth]{image/herausforderungen2_nocolor.PNG}
	\caption{Zusammenfassung der Herausforderungen anhand einer Matrix}
	\label{fig:herausforderungen2}
\end{figure}

Bisher wurden die Maßnahmen unabhängig ihres Bereiches gewertet.
Die Herausforderungen im \gls{devops}-Bereich betreffen hauptsächlich Optimierungen der Pipeline.
Durch die Lösung dieser Probleme können Serverressourcen eingespart und die Fertigstellung des Buildprozesses beschleunigt werden.
Die meisten Serverressourcen werden bei \textit{LichtBlick} als \textit{Infrastructure as a Service} von Azure bereitgestellt.
Da eigene \textit{Runner} verwendet werden sollen, kann die Reduzierung von verwendeter Serverzeit Kosten einsparen. 
Außerdem kann durch die Beschleunigung des Buildprozesses die Rückmeldung von Ergebnissen an die Entwickler und \gls{qa}-Beauftragen schneller erfolgen.

Im \gls{qa}-Bereich betreffen die Herausforderungen die Erstellung der Testfälle sowie die Verwendung der Berichte.
Der Fokus dieser Arbeit liegt auf der Identifikation von Herausforderungen von \gls{dast} in \gls{cicd}-Pipelines und spricht somit im Wesentlichen den \gls{devops}-Bereich an.
Jedoch ist der Kernschwerpunkt für diese Thematik die Testerstellung und Umgang der Sicherheitsberichte.
Aus diesem Grund können die Maßnahmen des \gls{qa}-Bereichs im Vergleich zum \gls{devops}-Bereich mit einer gering erhöhten Priorität versehen werden. 
Somit empfiehlt es sich nach ersten Integrationsschritten von \gls{dast} in \gls{cicd}-Pipelines den Fokus auf den \gls{qa}-Bereich zu setzen.

\section{Alternativen}
\label{sec:alternatives}
In dieser Arbeit wurde eine eigene Struktur zur Integration von \gls{dast}-Techniken in \gls{cicd}-Pipelines nach bewährten Verfahren erstellt.
Es gibt jedoch auch alternative Lösungen.
\textit{GitLab} stellt z.B. eine fertige \gls{dast} Lösung bereit, welche sich in die drei Analysatoren \textit{proxy-based analyzer}, \textit{browser-based analyzer} und \textit{api analyzer} aufteilen lassen \autocite{gitlab_dynamic_2023}.

Der \textit{proxy-based analyzer} soll sich für statische \gls{html} Webanwendungen eignen und verwendet \gls{owasp} \gls{zap} wie in der Fallstudie bei \gls{wast} Scanning.
Standardmäßig wird dort ebenfalls das \textit{zap-baseline.py} Skript ausgeführt.

Der \textit{browser-based analyzer} wurde von \textit{GitLab} selbst entwickelt.
Dort werden die Scans direkt im Webbrowser anstatt der Verwendung eines Proxys ausgeführt.
Dies soll insbesondere bei \textit{Single-Page Applications}, welche viel \gls{js} verwenden, die Performance verbessern.
Ähnlich wie beim vollständigen \gls{zap} Scan, wird die Webanwendung nach weiteren Navigationspfaden durchsucht, diese aufgerufen und während des \textit{Crawl}-Prozesses die Ergebnisse mittels Scanner nach Schwachstellen untersucht.
Außerdem können vorab Authentifikationsverfahren und abschließend aktive Scans ausgeführt werden.  

\gls{api}s unterschiedlicher Technologien wie GraphQL, REST oder SOAP können vom \textit{api analyzer} nach Schwachstellen gescannt werden.
In dieser Arbeit wurde die konkrete Analyse einer \gls{api} nicht durchgeführt.
Aber auch hierzu kann ein vordefiniertes \gls{zap} Skript verwendet werden.
Der Aufbau und die Durchführung sollte ähnlich wie beim \gls{wast} Scanning sein.

Dieses von \textit{GitLab} zur Verfügung gestelltes \gls{dast} aber auch weitere Sicherheitsfeatures sind exklusive Inhalte des kostenpflichtiges Ultimate Abonnements.
Im Kern müssen diese \gls{dast} Features von \textit{GitLab} genauer analysiert werden, um einen konkreten Vergleich zwischen der durchgeführten Fallstudie und dieser Alternative zu ermöglichen.
Dennoch lässt sich abschätzen, dass die Verwendung der Alternative keinen signifikanten Mehrwert bietet.
Die verwendeten Technologien hinter dem \textit{proxy-based analyzer} und \textit{api analyzer} werden zum Großteil ebenfalls über \gls{zap} abgebildet, welches in der Fallstudie bereits integriert ist.
Durch die Verwendung eigener Images ist eine bessere Kontrollmöglichkeiten der Scans und Sicherheitstests gegeben.
Außerdem werden mit der alternativen Lösungen keine \gls{sas}-Techniken abgebildet, welche jedoch nach den bewerteten Ergebnissen dieser Fallstudie den meisten Mehrwert bieten.
Aus diesen Gründen wird vorerst davon abgeraten, die exklusiven \gls{dast} Lösungen von \textit{GitLab} zu verwenden bevor keine eigene Lösung entwickelt wurde, welcher dieser Fallstudie nachempfunden wurde.