# Zeitplan
- [ ] **Fr. 09.12**: Glattziehen bis *Abschnitt 2.2*
  - [x] LichtBlick in Problemstellung einbeziehen
  - [x] Quellen und Schrift nachbessern
  - [x] Abbildungen integrieren
  - [ ] Quellen in CICD und Docker nachziehen
- [x] **Wochenende / 10. - 11.12**: *Abschnitt 2.2* fertig stellen 
  - [ ] Überbügeln
- [x] **Mo. - Mi. / 12. - 14.12**: *Beschreibung der Tools* fertig stellen
- [ ] **Do. - Fr. / 15. - 16.12**: *Related Work* ergänzen und nachbessern
- [x] **Wochenende / 17. - 18.12**: Projekt BDST und SAS integrieren + Experiment Aufbau beschreiben
- [ ] **Mo. - Fr. / 19. - 23.12**: Experiment Kapitel schreiben
- [ ] **Weihnachtsbumms.. 24. - 26.12**: Nachbesserungen und Vergleich?
- [ ] **Di. - Fr. / 27. - 30.12**: Experiment erweitern; Vergleich integrieren?? oder mehr auf Toolmöglichkeiten eingehen.. Prozesse und Workflows optimieren.. wie gehen Entwickler mit fehlerhaften Build um <- muss technisch umgesetzt werden damit es in die Methodik passt; Experiment zuende schreiben
- [ ] **Neujahresbumms.. 31. - 01.01**: -
- [ ] **Mo. - Fr. / 02. - 06.01**: Diskussion
- [ ] **Sa. - So. / 07. - 09.01**: Fazit
- [ ] **Ab 09.01, 2 Wochen Zeit für**: Nachbesserungen und Reviews
- [ ] **Ab 23.01**: Thesis binden lassen


