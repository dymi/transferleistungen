# TODO:

Anmerkungen: 
* Generell (nicht unbedingt an dieser Stelle) kann man irgendwo anbringen, dass bereits eine automatische Erkennung von Schwachstellen in verwendeten Bibliotheken in der Pipeline existiert (OWASP Dependencycheck mit Sonarcloud)

    Letzteres könnte ggf. auch für den Ausblick am Ende sinnvoll sein, dass auch die Ergebnisse Deiner Arbeit dort automatisiert gespeichert werden können.

    Healthchecks sind ja auch eher weniger IT-Sicherheitsrelevant, sondern zeigen ja primär dass der Dienst noch läuft.

* Mir fällt auf, dass Du nirgends PRs erwähnts. IMHO solltest Du das tun, da Du hier auch zeigen kannst, dass bereits in diesem Schritt fachliche Diskussionen erfolgen können und z.B. fehlgeschlagene Unittests gar nicht erst im Master landen können (also nicht per Notification (es ist was schief gelaufen), sondern anders herum, nur wenn alles ok, gehts überhaupt weiter

    Auch dass für PRs eine eigene Pipeline möglich ist (was wir ja auch machen), die z.B. erst mal nur Dinge prüft, die kritisch/schnell zu prüfen sind könnte sinnvoll sein

* Spezifikation BDST Testkonzept: Sätze -> **Der Job gilt als... bestanden mit Fehlern deklariert.** überarbeiten

* H1 - Eigene Runner:  **könnten somit Geheimnisse von privaten Repositories
gestohlen werden** -> Jein :)
Wenn zum Builden z.B. Zugriff auf ein geschütztes NuGet-Package notwendig (jFrog) ist, dann muss der Runner ja die Konfiguration erhalten um dieses aufrufen zu können. Somit spricht das zusätzlich gegen einen öffentlichen Runner

    Ggf. auch klarstellen, dass das Repo an sich zwar keine Secrets enthält, aber an sich ja ein Geschäftsgeheimnis ist, welches man nicht weitergeben möchte

