# Bachelor-Thesis
[![pipeline status](https://gitlab.com/dymi/transferleistungen/badges/master/pipeline.svg)](https://gitlab.com/dymi/transferleistungen/-/commits/master)
## [Dynamic Application Security Testing: Integration von automatisierten Sicherheitstests in CI/CD-Pipelines](Bachelor/Latex/transfermodule.pdf)

# Transferleistungen (2019-2023)
## Nr.: 1 [Entwicklung   einer   Lösung   zur   Aufspaltung   und Verteilung von EDIFACT-Nachrichten in das Bestands-und Neusystem](TL1/Transferleistung_1.pdf) 
* (16.04 - 13.05.2020)
* <u>Modul:</u> Einführung in die Programmierung
* [Feedback](TL1/feedback.md):
Doch Sie haben bei einer TFL die Aufgabe, eine wissenschaftliche (!) Hausarbeit zu verfassen, keinen reinen Bericht. Es fällt auf, dass Sie <b>kaum Literatur benutzen und keine aktuellen Studien</b>. Mir macht es den Anschein, als hätten Sie nicht verstanden, was wissenschaftliches Arbeiten wirklich bedeutet. Bitte beschäftigen Sie sich noch einmal grundlegend damit. 
* <i><b>7/10 Punkte!</b></i>
  
## Nr.: 2 [Migration von linearen on-premises Systemen zu parallelen Prozessen in der Cloud](TL2/TL2.pdf) 
* (04.01. - 01.02.2021)
* <u>Modul:</u> Einführung in die objektorientierte Programmierung 
* [Feedback](TL2/Feedback.PNG): Verzeichnisse vor die Einleitung. Quellenangabe nach Punkt gilt für gesamten Absatz. Vor dem Punkt nur für den Satz.
* <i><b>10/10 Punkte!</b></i>
  
## Nr.: 3 [Analyse von Umfang und Laufzeitverhalten komplexer Datenabfragen mittels GraphQL gegenüber RESTful implementierten Services](TL3/TL3.pdf) 
* (14.10. - 09.11.2021)
* <u>Modul:</u> Praxis der Softwareentwicklung
* [Feedback](TL3/Feedback.PNG):
Ich hätte mir in Kapitel 1 gewünscht, dass Sie eine grobe Struktur der Arbeit aufstellen.
Ab dem Literaturverzeichniss sollten die Seitenzahlen wieder römisch sein.
* <i><b>10/10 Punkte!</b></i>

## Nr.: 4 [Bloomfilter: Analyse zur Reduzierung von API Anfragen mittels Einsatz eines probabilistischen Zwischenspeichers](TL4/Latex/transfermodule.pdf) 
* (18.03. - 14.04.2022) 
* <u>Modul:</u> Praxis der Softwareentwicklung
* [Feedback](TL4/feedback.PNG):
* <i><b>10/10 Punkte!</b></i>

## Nr.: 5 [Performancevergleich des Kaltstartverhaltens von Azure Functions mit unterschiedlichen Technologien](TL5/Latex/transfermodule.pdf) 
* (30.05. - 27.06.2022) 
* <u>Modul:</u> Praxis der Softwareentwicklung
* [Feedback](TL5/feedback.PNG):
* <i><b>10/10 Punkte!</b></i>

## Nr.: 6 [Rate Limits in Cloud Services: Abwehr gegen Bot Angriffen auf öffentliche APIs ](TL6/done/TL6.pdf) 
* (10.08. - 01.09.2022) 
* <u>Modul:</u> IT-Sicherheit
* [Feedback](TL6/done/feedback.PNG):
 Es reicht nicht aus, sich auf frühere Quellen/Text Passagen zu beziehen. Hier würde ich eine Angabe erwarten "Zur Umsetzung von Drosselungsstrategien gibt es nach Microsoft folgende Möglichkei- ten" Bei Print-Quellen fehlt die Seitenangabe in den Kurzbelegen z. B. (Wadhwa und Mandhar 2021) oder (Baig, Sait und Binbeshr 2016) Eine Seitenangabe im Literaturverzeichnis ist nicht ausreichend, da Quellen ja mehrfach verwendet werden und deren Inhalte sich auf unterschiedlichen Seiten befinden können. Es muss immer für den Leser nachvollziehbar sein, ohne großartiges Suchen.
* <i><b>10/10 Punkte!</b></i>

# Weitere Hausarbeiten und Projekte
## [Einführung in KI - Eine Klassifizierung von Hatespeech anhand eines einzelnen Features](WeitereHausarbeiten/hatespeech.pdf) (19.12.2020)
[Feedback](WeitereHausarbeiten/hatespeech_Bewertung.pdf):
Die Einleitung ist etwas knapp und es werden nur wenige Grundlagen geklärt. Die eigentliche Zielgruppe der Arbeit, Absolvent:innen des B.Sc.  Studiengangs, werden etwas zu wenig adressiert und es wird zu viel Vorwissen bei Lesenden vorausgesetzt. Related Works -> Related Work (keine Mehrzahl).
<br><i><b>Note 1,0!</b></i>

## [Simulation von elektronischen Schaltungen](WeitereHausarbeiten/simulationhazards.pdf) (07.11.2021)
<i><b>Note 1,3!</b></i>

## [Green IT - Der CO2-Fußabdruck der IT](WeitereHausarbeiten/greenit.pdf) (22.08.2022)
<i><b>Note 3,0!</b></i>

## [Usability Engineering - Scenario-Based Development des digitalen Vokabeltrainers Phase6](WeitereHausarbeiten/uimodul.pdf) (10.10.2022)
<i><b>Note 1,0!</b></i>

## [WannaCry](WeitereHausarbeiten/pentesting.pdf) (12.11.2022)

## [Abschluss Software Projekt: Publication-Management (GitHub)](https://github.com/killi199/Publication-Management) (21.11.2022)
