\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{naklatex}

\LoadClass[
	numbers=noenddot,
	bibliography=totoc,
	index=totoc,
	listof=totoc,
	fontsize=12pt,
	headings=small,
	parskip=half
]{scrbook}

\RequirePackage{scrhack} % package for compatibility with listings
\RequirePackage{listings} % code formatting
\RequirePackage{xstring} % string comparison (transfermodule course of study)
\RequirePackage[ngerman]{babel} % deutsches Sprachpaket
\RequirePackage[utf8]{inputenc} % utf8 Support
\RequirePackage[babel, german=quotes]{csquotes}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern} % Gerenderte Schrift
\renewcommand{\rmdefault}{ptm}
\RequirePackage{graphicx}
\RequirePackage{microtype}
\RequirePackage{float}
\RequirePackage{eurosym} % Euro Symbol
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{pdfpages}
\RequirePackage{tabularx}



% Seitenränder
\RequirePackage[a4paper,top=20mm,bottom=20mm,left=20mm,right=20mm]{geometry}

% Zielenabstand 1,25
\RequirePackage{setspace}
\spacing{1.25} 
\setlength{\parskip}{0.5em}
\addtolength{\footskip}{-5mm} 
\pagestyle{headings} 	
\newcounter{savepage}

%load hyperref last but before glossaries
\RequirePackage[hyphens]{url} %break urls
\RequirePackage[hidelinks]{hyperref}
\hypersetup{bookmarksnumbered}

\RequirePackage[automake,nonumberlist,acronym,toc]{glossaries}
\makeglossaries

\ProcessOptions\relax