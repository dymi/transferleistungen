\chapter{Experiment}
\label{chap:experiment}
In diesem Kapitel wird ein \gls{ohbf} implementiert, welche sich auf den in \autoref{chap:einleitung} referenzierten Sachverhalt bezieht.
Dafür wird der Aufbau der Implementierung aufgezeigt und die Durchführung des Experiments geschildert.
Anschließend werden auf besondere Beobachtungen der Ergebnisse eingegangen.

\section{Aufbau}
\label{sec:aufbau}
Die im Unternehmen entwickelten Softwareprodukte sind meistens in C\# geschrieben, weshalb auch der \gls{ohbf} Prototyp in dieser Programmiersprache entwickelt wurde.
Für die \textit{lookup3} Hashfunktion wurde die etablierte Data.HashFunction Bibliothek vom Amazon Web Services Entwickler Brandon Dahler verwendet, welche 2022 ihr letztes Update erfuhr \autocite{dahler_datahashfunction_2022}. 

Die Klasse \textit{Ohbf} benötigt zur Initialisierung die geschätzte Anzahl der langfristig abzubildenen Elemente und eine vordefinierte \gls{fpr}.
Anschließend wird mathematisch die Parameter $m$ und $k$ bestimmt (s. \autoref{lst:getmAndK}).
Mit diesen Parametern werden automatisch die Partitionslängen bestimmt und die einzelnen Partitionen als Bit Arrays generiert (s. \autoref{lst:generatebitarray}).
In \autoref{lst:ctor} ist ein Auszug der \gls{ohbf} Klasse zu sehen, wo dieser Prozess im Konstrukor der Klasse definiert ist.\\


\begin{lstlisting}[caption={Codeauszug vom Konstrukor der OHBF Klasse},label={lst:ctor},captionpos=b,style=csharp, xleftmargin=0.05\textwidth,xrightmargin=0.05\textwidth][H]
public class Ohbf
    {
        private BitArray[] _bitArrays;

        public Ohbf(int n, float fpr)
        {
            var m = GetSize(fpr, n);
            var k = GetHashcount(m, n);
            GenerateBitArrays(m, k);
        }
    ...
\end{lstlisting}


Beim Erstellen der Bit Arrays werden stets $k$ verschiedene Partitionen gebildet.
Damit bei der Modulooperation für die jeweiligen Partitionen von einander unabhängige und möglichst verschiedene Ergebnisse raus kommen, werden die Partitionsgrößen mit Primzahlen festgelegt.
Da die Summe von diesen Partitionsgrößen $m_f$ nur selten dem berechneten $m$ entsprechen, wird zur Bestimmung der jeweiligen Partitionsgrößen ein Nährungsverfahren gewählt, sodass die Differenz $|m - m_f|$ möglichst gering ist.
Dies hat nach Lu et al. kaum Auswirkung auf die \gls{fpr}.\\


\begin{lstlisting}[caption={Codeauszug zur Berechnung der Partitionslängen},label={lst:plength},captionpos=b,style=csharp, xleftmargin=0.05\textwidth,xrightmargin=0.05\textwidth][H]
    private IEnumerable<int> DeterminePartitionlenghts()
        {
            var r = Convert.ToInt32(_m / _k);
            var pTable = GeneratePrimes(r + 300);
            var closestPrime = pTable.OrderBy(x => Math.Abs((long) x - r)).First();
            var idx = pTable.IndexOf(closestPrime);
            var sum = 0;
            for (var i = idx - 1; i >= idx - _k; i--)
                sum += pTable[i];
            
            var min = Math.Abs(sum - _m);
            
            while (true)
            {
                sum += pTable[idx] - pTable[idx - _k];
                var diff = Math.Abs(sum - _m);
                if (diff >= min) break;
                min = diff;
                idx += 1;
            }
            for (var i = 1; i <= _k; i++)
                yield return pTable[idx - i];
        }
\end{lstlisting}


\autoref{lst:plength} zeigt einen Codeauszug zur Berechnung der Partitionslängen vom \gls{ohbf} Prototypen. 
Zum vereinfachten Verständnis des Algorithmus zeigt \autoref{lst:konsole} eine Konsolenausgabe mit einer beispielhaften Konfiguration mit $m=100$ und $k=3$.

Der Algorithmus ist hierbei angelehnt an die vorgestellte Berechnung von Lu et al., welche für detailliertere Informationen sowohl in deren Arbeit \textit{One-Hashing Bloom Filter} als auch in Pseudocode Darstellung in \autoref{fig:partition} nachzulesen ist.
Die Ergebnisse der Partitionslängen sind identisch wie in \autoref{tab:mdiff}.

\autoref{tab:paraverh} zeigt die Ergebnisse mittels des Prototypen berechneten Parameter $mf$ und $k$ für verschiedene \gls{fpr} mit $n=1,5\text{e}6$. 
Wie das zugehörige Diagramm in \autoref{fig:verh} zeigt, sinkt bei steigender \gls{fpr} die benötige Größe und Partitionen des \gls{bf} proportional zueinander.
Bei einer anzustrebenden niedrigen \gls{fpr} wird entsprechend viel Speicher benötigt und die Komplexität sowie benötigte Rechenleistung nimmt zu.  

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{image/verhaeltnispara}
	\caption{Verhältnis der Parameter $fpr$, $mf$ und $k$}
	\label{fig:verh}
\end{figure}

Anschließend verfügt die Klasse \textit{Ohbf} über zwei öffentliche Methoden \textit{Add} und \textit{InCache} (s. \autoref{lst:operationen}).

\section{Durchführung}
\label{sec:durchf}
Die Anzahl der abzuspeichernden Elemente lassen sich auf 30 Millionen Datensätze beschränken. 
Dies ergibt sich daraus, dass im \gls{crm} System Vertragsdaten geschrieben werden, welche sich pro Vertrag auf lediglich einen einzigen komplexen Datensatz abbilden lässt.
Das Unternehmen verzeichnet nach insgesamt 20 Jahren knapp eine Million Kunden.
Zukunftsgerichtet kann die Anzahl der Verträge auch in einem kurzen Zeitraum rapide steigen, wie durch eine größere Anzahl an Kundenübernahmen von anderen konkurrierenden Unternehmen.
Entsprechend wird mit 1,5 Millionen Kunden gerechnet.
Kunden können auch mehrere Verträge haben. 
Nach Auswertung von Unternehmensdaten kann $n$ mit $1,5\text{e}6*2$ auf 3 Millionen Vertragsdaten geschätzt werden.
Bei der Datensynchronisation werden nicht nur neue Kunden hinzugefügt sondern auch bereits bestehende Kundendaten kontinuierlich aktualisiert.
Anhand der Datenhistorien ergibt sich ein geschätzter Durchschnittswert von zehn Änderungen pro Vertrag innerhalb einer gesamten Vertragslebensdauer.
Somit wird für den \gls{ohbf} Prototyp $n$ auf 30 Millionen geschätzt. 

Die Literatur sieht bei \gls{bf} eine \gls{fpr} von unter 1\% für die meisten Anwendungsfälle als akzeptabel an.
Für den Prototypen wird entsprechend eine \gls{fpr} von 1\% angenommen.\\

\begin{lstlisting}[caption={Codeauszug aus der Main Methode zur Simulation},label={lst:simulation},captionpos=b,style=csharp, xleftmargin=0.05\textwidth,xrightmargin=0.05\textwidth, firstnumber=1][H]
    ...
    foreach (var (n, fpr) in configs)
    {
        long falsePositives = 0;
        var testdataCount = n * 100;
        for (var j = 0; j < 100; j++)
        {
            var bloomfilter = new Ohbf(n, fpr);
            var customers = DataGenerator.GetData(testdataCount);
            var enumerator = customers.GetEnumerator();

            for (var i = 0; i < n; i++)
            {
                enumerator.MoveNext();
                var customer = enumerator.Current;
                bloomfilter.Add(customer.FormatToString());
            }

            enumerator.Dispose();
            var inCache = customers.Count(c => bloomfilter.InCache(c.FormatToString()));
            falsePositives = inCache - n;
        }

        var fprPrototyp = (float) falsePositives / testdataCount;
        var fprDiff = Math.Abs(fprPrototyp - fpr);
        ...
\end{lstlisting}


Für das Testen des Prototypens wurde eine Simulation mit unterschiedlichen $n$ durchgeführt.
Die Simulation ist für jedes unterschiedliche $n$ 100 mal durchlaufen.
Dabei wurden $n*100$ gemockte Kundendaten erzeugt und $n$ Kundendaten dem \gls{bf} hinzugefügt.
Anschließend wurde mittels der \textit{InCache} Methode des \gls{ohbf} alle $n*100$ Kundendaten geprüft, ob diese sich im Cache befinden.
Die daraus resultierende Rate wurde mit der gegebenen $0.01$ Rate gegenübergestellt.
Dies soll mit einer minimalen Differenz bestätigen, dass die erwartete \gls{fpr} erfüllt ist.
Außerdem wurden die benötigten Rechenzeiten der jeweiligen Operationen wie \textit{Add}, \textit{InCache} und die Initialisierung eines \gls{ohbf} in Millisekunden berechnet.
\autoref{lst:simulation} zeigt einen Codeauszug der Simulation, wobei hier zur Übersicht die Zeitmessungen und Konsolenausgaben entfernt wurden.


\begin{table}[H]
    \setlength{\arrayrulewidth}{0.5mm}
    \setlength{\tabcolsep}{18pt}
    \renewcommand{\arraystretch}{1.5}
    \centering
    \begin{tabular}{p{2cm}|p{2cm}|p{2cm}|p{2cm}|p{2cm}}
    \hline
        $n$ & $m$ & fpr$_{diff}$ & $f_{Add}(n)$ & $f_{InCache}(n*100)$  \\ \hline
        100 & 960 & $8,0\text{e-}4$ & 0,054 & 4,83 \\ 
        1000 & 9594 & $1,3\text{e-}2$ & 0,442 & 39,29 \\ 
        10000 & 95832 & $3,7\text{e-}5$ & 3,981 & 341,67  \\ 
        100000 & 958502 & $6,3\text{e-}5$ & 37,18 & 3345,33  \\ 
        1000000 & 9585090 & $4,7\text{e-}4$ & 386,549 & 34936,62 \\ \hline 
    \end{tabular}
    \caption{Durchschnittswerte der Simulation mit 100 Durchläufe für verschiedene $n$ mit $k=0,01$}
    \label{tab:exp}
\end{table}

Die Initialisierungszeit eines \textit{Ohbf} Objekts liegt bei einer \gls{fpr} von $0.01$ mit $n=100$ lediglich bei $0.017 ms$.
Bei der Initialisierung mit $n=1,0\text{e}6$ liegt der Wert hierbei bei $0.34 ms$, welches keine erhebliche Steigung dar stellt.
Die Durchschnittswerte der Simulation sind aus \autoref{tab:exp} zu entnehmen.
Dabei ist zu berücksichtigen, dass bei der \textit{InCache} im Vergleich zu der \textit{Add} Operation hundert Mal so viele Daten geprüft werden. 
Unter dieser Berücksichtigung lässt sich schließen, dass die Durchschnittszeit dieser Operationen zueinander kaum einen Unterschied ausmacht. 

\section{Beobachtung}
\label{sec:beobachtung}
Anhand der gemessenen Werte aus \autoref{tab:exp} lässt sich die Proportionalität von $n$ und $m$ nach Starobinski et al. bestätigen. 
Hinzu kommt, dass die Rechenzeiten für die beiden Operationen \textit{Add} und \textit{InCache} ebenfalls proportional zu $n$ sind.
Somit lassen sich für größere $n$ Werte wie z.B. eine Milliarde die benötigte Zeit für das Hinzufügen dieser Daten schnell approximieren. 

Die Differenz des erwarteten und tatsächlichen \gls{fpr} liegt im Durchschnitt bei $5,3\text{e-}4$.
Dieser niedrige Wert bestätigt das Modell, da die implementierten Algorithmen keinen negativen Einfluss auf die \gls{fpr} haben sondern eher den Erwartungen entsprechen.

Die Methoden \textit{Add} und \textit{InCache} sind in \autoref{tab:exp} auf $n$ bzw. $n*100$ angewendet worden.
Durch entsprechende Division mit $n$ bzw. $n*100$ lässt sich im Schnitt eine Konstante Berechnungszeit von $3,8\text{e-}4$ dieser Operationen messen.
Dies bestätigt ebenfalls das Modell, dass die Operationen eines \gls{bf} ein Komplexitätsmaß von $O(1)$ aufweisen bzw. konstante Wachstumsfunktionen darstellen. 

Bei der Ermittlung der Partitionen und deren Längen, wird eine Primzahlentabelle benötigt. 
Während des Experiments ist aufgefallen, dass für besonders große $n$ wie z.B. eine Milliarde die Erstellung der Primzahlentabelle über konventionelle Herangehensweise zu lange dauert.
Ursprünglich wurde hierbei von zwei bis $x$ mit $x = m/k + \delta$ iteriert und jedes einzelne Element geprüft, ob es eine Primzahl ist wie \autoref{lst:generatePrimes} zeigt.
Da der Algorithmus in \autoref{lst:plength} jedoch lediglich eine Tabelle an Primzahlen um $x-k$ und $x+k$ benötigt, wurde ein Algorithmus eingeführt, der eine entsprechende Primzahlentabelle erstellt und somit schneller die Ergebnisse liefert (s. \autoref{lst:generatePrimes2}). 
Anderenfalls würde bei der Initialisierungszeit eines \textit{Ohbf} Objekts deutlich mehr Zeit benötigt werden.