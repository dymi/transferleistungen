\chapter{Related Work}
\label{chap:relatedwork}
In diesem Kapitel werden auf bereits vorhandene Forschungsarbeiten zum Bloomfilter und Hashingalgorithmen eingegangen. \\

\textit{A. Optimizing Bloom Filter: Challenges, Solutions, and Comparisons \autocite{luo_optimizing_2019}} \\

Luo et al. geben in ihrer Arbeit einen Überblick von bis zu 60 verschiedenen \gls{bf} Variationen. 
Der Schwerpunkt ihrer Arbeit seien hierbei Optimierungtechniken bzgl. der Performance und Generalisierung.
Zur Verbesserung der Performance widmen sich viele Variationen der Reduzierung der \gls{fpr} und der Implementierungskosten.
Außerdem kann durch Anpassung der verwendeten Datensätze als Eingabe und Erweiterungen der Funktionalitäten bzgl. der Abfrageoperation der \gls{bf} generalisiert werden.    
Das Zusammenspiel dieser Optimierungstechniken wird in \autoref{fig:luotopfour} zur Verdeutlichung aufgezeigt.\\

\textit{B. One-Hashing Bloom Filter \autocite{lu_one-hashing_2015}}\\

In der Arbeit von Lu et al. wird der \gls{ohbf} vorgestellt.
Dieser erlaubt die Kosten der Rechenzeit von Hashingalgorithmen einzusparen, indem statt $k$ viele Hashingfunktionen für ein Element lediglich nur noch eine Hashingfunktion benötigt wird. 

Die Hashfunktionen sollen für ein Element $x$ aus der Menge $A$ die jeweiligen Indexpositionen des Bit Arrays $B$ berechnen. 
Der Prozess des \gls{ohbf} Hashings wird in die Hash- und Modulophase unterteilt.
In der Hashphase, $A \rightarrow T$, wird das Element von $A$ nach $T$ mit einer Hashfunktion $h(x)$ transformiert, wobei $T$ ein temporärer Maschinenspeicher wie z.B. ein 32-Bit Integer dar stellt.
In der Modulophase, $T \rightarrow B$, wird das bereits \textit{gehashte} Element von $T$ nach $B$ mit einer Modulooperation $|B|$ (z.B. $h(x) \bmod m$) transformiert. Diese Phase ist notwendig, da das \textit{gehashte} Element aus $T$ i.d.R. größer ist als die Länge des \gls{bf} $m$.

\autoref{fig:schematic1} zeigt die schematische Sicht dieser zwei Phasen eines Standard Bloom Filters mit $k=3$ und $n=2$.
Dort ist zu erkennen, dass in der ersten Phase für jedes Element alle drei Hashfunktionen verwendet werden.
Für jede Transformation wird eine Modulooperation von gleicher Länge in der zweiten Phase durchgeführt. 
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{image/ohbf1}
	\caption{Schematische Sicht eines Standard Bloom Filters mit $k=3$ und $n=2$ nach Lu et al.}
	\label{fig:schematic1}
\end{figure}

Im Vergleich dazu zeigt der \gls{ohbf} in \autoref{fig:schematic2}, dass in der ersten Phase lediglich nur eine Hashfunktion für ein Element verwendet wird.
Der Speicher des \gls{ohbf} wird in $k$ verschiedene Partitionen gespalten. 
In der Modulophase wird das transformierte Objekt in verschiedene Bereiche der einzelnen Partitionen geschrieben. 
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{image/ohbf2}
	\caption{Schematische Sicht eines OHBF mit $k=3$ und $n=2$ nach Lu et al.}
	\label{fig:schematic2}
\end{figure}

Für eine beispielhafte Berechnung zur Verdeutlichung des Mechanismus eines \gls{ohbf} seien folgende Bedingungen gegeben:
\begin{itemize}
	\item $m_i$ mit $1 \leq i \leq k$ für die $i-te$ Partitionslänge
	\item $m=\sum_{i = 1}^{k}m_i$
	\item $k=3$
	\item $m_1=11, m_2=13$ und $m_3=15$
\end{itemize}
Es wird ein Element $x$ mit nur einer Hashfunktion berechnet, welche den Ausdruck $h(x)=4201$ annimmt. 
Nach den Berechnungen $h(x) \bmod m_1=10, h(x) \bmod m_2=2$ und $h(x) \bmod m_3=1$ wird das entsprechende 10., 2. und 1. Bit jeder Partition gesetzt.

Wie \autoref{tab:ohbf} zeigt, ist die Differenz der \gls{fpr} zwischen einem Standard \gls{bf} und einem \gls{ohbf} sehr gering. 
Sofern die Partitionsgrößen alle nahe beieinander liegen und $(m_i,m_j)=1,1 \leq i \le j \leq k$ gilt, kann entsprechend die Formel zur \gls{fpr} eines Standard \gls{bf} mit nur geringer Abweichung auch für den \gls{ohbf} verwendet werden.
Die Bestimmung der Partitionslängen wird in \autoref{sec:aufbau} näher erläutert.

Lu et al. haben bewiesen, dass die Hashingausgaben in der Modulophase voneinander unabhängig sind, sofern die Partitionslängen paarweise teilerfremd zuseinander sind.
Mit dieser Eigenschaft wird lediglich nur eine gute Hashfunktion für die Hashphase benötigt, um ein \gls{ohbf} zu implementieren.\\

\textit{C. Empirical Evaluation of Hash Functions for Multipoint Measurements \autocite{henke_empirical_2008}}\\

In der Arbeit von Henke et al. werden 25 Hashfunktionen, darunter 23 nicht-kryptografische und 2 kryptografische Hashfunktionen, auf ihre Eignung für eine hashbasierte Selektion analysiert. 
Die Hashfunktionen wurden anhand von verschiedenen Kriterien unter anderem der Performance bewertet.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{image/hashfunc.PNG}
	\caption{Performancevergleich nicht-/kryptografischer Hashfunktionen  nach Henke et al.}
	\label{fig:crypfunc}
\end{figure}
\autoref{fig:crypfunc} zeigt die Performance von den 25 verwendeten Hashfunktionen gemessen an der Rechenzeit für verschiedene Schlüssellängen in Bytes.
Während kryptografische Hashfunktionen verschiedene Sicherheitsaspekte nachweisen, sind nicht-kryptografische Hashfunktionen wesentlich schneller in der Berechnung von Hashwerten.

Abschließend kommen Henke et al. zu dem Ergebnis, dass nach Berücksichtigung aller Qualitätsmerkmal die nicht-kryptograpihsche Hashfunktion BOB (nach Bob Jenkins) bzw. \textit{lookup3} für hashbasierte Selektion am geeignetsten sei.

Des Weiteren bestätigen auch Arbeiten wie \textit{Performance of the most common noncryptographic hashfunctions} von Estébanez et al., dass \textit{lookup3} empfehlenswert und ohne weitere Mängel sei.
Außerdem verwenden Unternehmen wie Google, Oracle und Dreamworks diese Hashfunktionen in ihren Produkten.
\textit{lookup3} wird ebenfalls für die Implementierung von PostgreSQL, Linux, Perl, Ruby und Infoseek verwendet. \autocite{estebanez_performance_2014}

Da für diese Arbeit das Hashing bei einem Bloomfilter keine besonderen Sicherheitsaspekte benötigt sondern eher auf eine hohe Performance und zufallsbasierte Verteilung der Hashwerte angewiesen ist, wird in dieser Arbeit die nicht-kryptografische Hashfunktion \textit{lookup3} verwendet.
