\chapter{Einleitung}
\label{chap:einleitung}
Die Einleitung umfasst die Erläuterung der Problemstellung, sowie die Erstellung eines Lösungskonzepts und anschließende Definition von Forschungsfragen.

\section{Problemstellung}
\label{sec:problemstellung}
Unternehmen verwenden unterschiedlichste Systeme zur Abwicklung ihrer Geschäftsprozesse.
Ein \gls{erp} System automatisiert und verwaltet Geschäftsprozesse in diversen Bereichen wie z.B. Finanzen und Vertrieb \autocite{corporation_erp-definition_nodate}.
Ein \gls{crm} System dient zur Planung, Steuerung und Durchführung aller interaktiven Prozesse mit den Kunden \autocite{holland_definition_nodate}.
Um die Vorteile eines \gls{erp} und \gls{crm} Systems komplett auszuschöpfen, findet die Verwendung dieser beiden Systeme in vielen Unternehmen Anwendung \autocite{ruivo_defining_2014}.
Für gewöhnlich verwalten \gls{erp} und \gls{crm} Systeme ihre Daten in separaten Datenbanken \autocite{tomic_erp_2016}.
Entsprechend müssen relevante Daten für die Abwicklung verschiedener Geschäftsprozesse in beiden Systemen synchron sein.

Im Folgenden wird ein Prozess aufgezeigt, welcher für die Datensynchronisation zwischen diesen Systemen zuständig ist.
Hierbei werden die Daten aus dem \gls{erp} in das \gls{crm} System geschrieben bzw. aktualisiert.

Die Datensynchronisation wird über Netzwerkanfragen mittels Lese- und Schreibzugriffen über ein \gls{api} durchgeführt.
\gls{api}s sind Programmierschnittstellen, welche die Kommunikation zwischen verschiedenen Anwendungen und Systemen ermöglichen \autocite{noauthor_brief_nodate}. 

Der zu analysierende Prozess weist durchschnittlich 850.000 \gls{api}-Aufrufe an einem Tag auf.
\autoref{fig:apiaufrugeamtag} zeigt die verwendeten \gls{api}s an.
Während die Summe der schreibenden Zugriffe aus \textit{Update} und \textit{Upsert} ca. 210.000 \gls{api}-Aufrufe sind, stellen die restlichen Aufrufe ungefähr 667.000 lesende Zugriffe dar. 
Die lesenden Zugriffe sind nötig, damit vorab die Aktualität beider Daten überprüft werden kann. 
Falls die Datenstände identisch sind, muss keine Aktualisierung durchgeführt werden.

Das Problem solcher Prozesse ist die hohe Netzauslastung und Menge an \gls{api}-Aufrufe. 
Bei der aktuellen Netzauslastung für diesen Prozess zahlt das Unternehmen 1.600€ im Monat.  
Unter der Annahme, dass von allen Anfragen 25\% schreibende und somit relevante Zugriffe sind und im Monat effektiv 400€ dafür bezahlt wird, kosten lesende Zugriffe dem Unternehmen etwa 1.200€ monatlich an Netzwerkauslastung. 
Da die Datensynchronisation Zeit in Anspruch nimmt und im \gls{crm} System die Daten nicht immer aktuell sind, kann die Abwicklung von  Geschäftsprozessen fehlerbehaftet sein bzw. nur verzögert ausgeführt werden. 
Dies kann je nach Anwendungsfall auch zu weiteren Kosten führen.

Um die Kosten einsparen zu können, muss die Menge von lesenden Zugriffen reduziert werden.
Eine Möglichkeit die lesenden Anfragen zu reduzieren, besteht darin, einen Cache im Prozess der Datensynchronisation einzubinden. 

\section{Prozessanalyse}
\label{sec:prozessanalyse}
In diesem Abschnitt wird der aktuelle Prozess zur Datensynchronisation und anschließend ein mögliches Lösungskonzept dargestellt.

\subsection{Aktueller Prozess}
\label{subsec:aktprozess}
\autoref{fig:vorher} zeigt den aktuellen Prozess der Datensynchronisation, wobei die blauen Pfeile dem Datensatz \textit{x} und die grünen Pfeile dem Datensatz \textit{y} zugeordnet sind.
Ein Pfeil symbolisiert eine Datenübertragung bzw. ein \gls{api} Aufruf.
Bei der Datensynchronisation delegiert das \gls{erp} System seine einzelnen Kundendaten an einen \textit{EventHandler-Service}.
Dieser ist dafür zuständig zu überprüfen, ob der gegebene Datensatz bereits im \gls{crm} System vorhanden ist, und diesen nach entsprechender Prüfung zu aktualisieren. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{image/vorher}
	\caption{Aktueller Prozess der Datensynchronisation}
	\label{fig:vorher}
\end{figure}

Der Kundendatensatz $x$ sei bereits in beiden Systemen identisch vorhanden.
Vereinfacht dargestellt, wird bei diesem Schritt drei Operationen durchgeführt.
Das \gls{erp} System delegiert den Datensatz \textit{x} an den EventHandler, dieser überprüft mittels eines Lesezugriffes, ob der Datensatz bereits im \gls{crm} System vorhanden ist. 
Diese Anfrage wird positiv bestätigt und die Datensynchronisation endet für den Datensatz $x$.

Der Kundendatensatz $y$ sei nur im \gls{erp} System.
Hier gelten die selben Schritte wie zuvor, nur dass nach einer negativen Antwort vom \gls{crm} System zusätzlich eine schreibende Operation durchgeführt wird.

\subsection{Lösungskonzept}
\label{subsec:lkonzept}

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{image/nachher}
	\caption{Soll-Prozess der Datensynchronisation}
	\label{fig:nachher}
\end{figure}

\autoref{fig:nachher} zeigt den Soll-Prozess der Datensynchronisation mit einem probabilistischen Zwischenspeicher  im \textit{EventHandler-Service}.
Dieser Zwischenspeicher kann aufgrund spezieller Algorithmen und Hashingverfahren mit einer hohen Wahrscheinlichkeit voraussagen, ob ein beliebiges Element auch Element der abgebildeten Menge $A$ sei.

Wie im vorherigen Beispiel wird $x$ an den \textit{EventHandler} delegiert.
Dieser überprüft jedoch vorerst im Cache, ob $x$ bereits bekannt sei, welcher vom Cache positiv bestätigt wird.
Aufgrund eines möglichen Falsch Positiv Ergebnisses, wird wie im aktuellen Prozess verfahren.
Dies heißt, obwohl $x \in A$ gilt, dass der Cache nicht zu 100 Prozent vorhersagen kann, ob diese Annahme tatsächlich wahr ist.

Wenn jedoch wie im Falle von $y$ der Datensatz nicht im Cache ist, braucht kein zusätzlicher Lesezugriff durchgeführt werden. 
Der Datensatz kann direkt in das \gls{crm} System geschrieben werden.
Dies ist nur möglich sofern Falsch Negative Ergebnisse ausgeschlossen sind.
Der Cache muss somit mit einer 100 prozentigen Wahrscheinlichkeit ausschließen, wenn ein Element nicht zur abgebildeten Menge gehört.

Sei $z$ ein weiterer Kundendatensatz, welcher in der \autoref{fig:nachher} mit roten Pfeilen dargestellt wird und der Cache liefert in diesem Fall ein Falsch Positives Ergebniss, wird mit $z$ genauso verfahren wie mit $y$ im aktuellen Prozess.

Insgesamt nimmt die Komplexität des Prozesses zu.
Wenn die Operationen des Caches jedoch sehr schnell sind und die \gls{fpr} niedrig ist, dann können somit alle Lesezugriffe von Kundendaten, welche sich nicht im \gls{crm} befinden, eingespart werden.

\section{Forschungsfragen}

Da Kundendaten umfassend sind und das Caching von über einer Million Vertragsdaten nicht praktikabel ist, empfiehlt es sich eine platzsparende und effiziente Cachingmethode zu implementieren.

In dieser Arbeit wird der \gls{bf}, ein probabilistischer Zwischenspeicher, analysiert und ein Prototyp entwickelt, der auf diesen Prozess abgestimmt ist. 
\gls{bf} und weitere Variationen wie der \gls{cbf} sind in bekannten Softwareprodukten implementiert, wie das von Facebook verwendete verteilte Datenbankverwaltungssystem Cassandra \autocite{lakshman_cassandra_2010}, dem Google Web Browser Chrome \autocite{scott_transition_2012}, der Online-Publikationsplattform Medium \autocite{talbot_what_2015}  und dem Netzwerk-Speicher-System Venti \autocite{quinlan_venti_2002}.

Es stellen sich folgende Forschungsfragen:
\begin{itemize}
	\item Wie arbeitet ein probabilistischer Zwischenspeicher? [F1]
	\item Welche Hürden und Aufwände sind bei der Einführung eines Bloomfilters zu berücksichtigen? [F2]
	\item Welche Varianten eignen sich für den aktuellen Anwendungsfall besonders? [F3]
\end{itemize}
