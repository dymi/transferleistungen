\chapter{Grundlagen}
\label{chap:grundlagen}
In diesem Kapitel wird die erste Forschungsfrage F1 beantwortet, welches die Basis und Grundlage der Arbeit darstellt. 
Dabei wird die Verwendung von Caches geklärt sowie auf deren Vor- und Nachteile eingegangen. 
Anschließend werden die Eigenschaften des Bloomfilters beschrieben.

\section{Caching}
\label{sec:caching}
Caching ist eine Technik zum Reduzieren von Datenanfragen durch Vorabrufen von Inhalten in Speicher bei den Endbenutzern.
Dadurch werden Zugriffszeiten auf Informationen beschleunigt. \autocite{maddah-ali_fundamental_2014}

Für verschiedene Anwendungsfälle wird Caching verwendet wie z.B. für Datenbanken, Content Delivery Networks, Hypertext Transfer Protocol Sitzungsverwaltungen, Web-Caching und \gls{api}s.
Die Branchen sind hierbei ebenfalls vielfältig wie z.B. für mobile Anwendungen, Internet of Things, Werbetechnologien, Gaming, Medien und E-Commerce.
Caches können diverse Daten wie Ergebnisse von rechenintensive Berechnungen, \gls{api}-Anfragen/Antworten und Web-Artefakte wie Hypertext Markup Language und JavaScript enthalten. \autocite{noauthor_caching_nodate}

Die Hauptvorteile bei der Verwendung von Caches liegen bei einer verbesserten Anwendungsleistung durch schnellere Zugriffszeiten, Reduzierung der Netzwerklast durch geringere Datenanfragen und entsprechend Senkung von Systemkosten. 
Veraltete Daten und somit fehlerhafte Benutzung oder Darstellung von Inhalten sowie eine Überlastung des Speichers vom Endbenutzer sind mögliche Nachteile von Caches.

\section{Bloomfilter}
\label{sec:bloomfilter}
In diesem Abschnitt werden die Grundlagen und Theorien des Bloomfilters in Hinsicht der Struktur, Merkmale und Herausforderungen erläutert.

\subsection{Struktur}
\label{subsec:struktur}
Ein \gls{bf} ist eine speichereffiziente probabilistische bzw. auf Wahrscheinlichkeit basierende Datenstruktur zur Repräsentation von Elementen eines Datensatzes.
Der Filter kann mit verschiedenen Hashfunktionen und einer akzeptablen \gls{fpr}, die Zugehörigkeit eines Elements zum Datensatz bestimmen. 
Dabei werden Elemente einer Menge gehasht und auf ein Bit Array abgebildet.
Dadurch verbrauchen \gls{bf} besonders wenig Speicherplatz.
Außerdem sind Falsch Negative Ergebnisse ausgeschlossen. \autocite{bloom_spacetime_1970}

Sei $A=\{x_1,x_2,...,x_n\}$ eine Menge aus $n$ Elementen, wobei der \gls{bf} alle $n$ Elemente über ein $m$-stelliges Bit Array repräsentiert. 
Alle $m$ Bits im Array werden initial auf 0 gesetzt. 
Beim Hinzufügen eines neuen Elements $x$ aus der Menge $A$ durchläuft das Element eine Gruppe aus $k$ unterschiedlichen und voneinander unabhängigen Hashfunktionen.
Diese Hashfunktionen $\{h_1,h_2,...,h_k\}$ ermitteln verschiedene Indexpositionen für das Bit Array und setzen an diesen Positionen den Wert auf 1, wobei stets $h_i(x)\in [0,m-1]$ gilt.
Um zu überprüfen, ob ein Element zur Menge $A$ gehört, werden die $k$ Hashfunktionen auf das Element angewendet und anhand der ermittelten Indexpositionen mit dem Bit Array verglichen.
Wenn beim Vergleich der Indexpositionen eine 0 detektiert wird, kann ausgeschlossen werden, dass dieses Element sich in der Menge $A$ befindet.
Wird beim Vergleich der Indexpositionen ausschließlich die 1 detektiert, wird davon ausgegangen, dass dies ein Element der Menge $A$ sei.
Dies kann entsprechend auch zu Falsch Positiven Ergebnissen führen.
Das Löschen eines Elements ist nicht erlaubt, da das Zurücksetzen der entsprechenden Einsen auf Nullen zu Falsch Negativen Ergebnissen für andere Elemente führen kann. \autocite{luo_optimizing_2019}

\autoref{fig:bfexample} zeigt ein Beispiel eines \gls{bf} mit $m=10$ Bits und $k=3$ Hashfunktionen, um die Menge $A=\{x_1,x_2,x_3\}$ zu repräsentieren.
Im ersten Schritt werden alle Elemente aus der Menge dem \gls{bf} hinzugefügt. 
Die drei Hashfunktionen ermitteln für das Element $x_1$ die Indexpositionen 0, 3 und 5.
An diesen Stellen wird der Wert des Indexes entsprechend auf 1 gesetzt.
Die Abfrage für $x_4$ ergibt \textit{Negative}, da mindestens ein Wert, nämlich der vom Index 2, auf 1 steht.
Die Indexwerte vom Element $x_5$ befinden sich alle auf 1. 
Somit liefert der \gls{bf} ein positives Ergebnis, obwohl $x_5 \notin A$ gilt.
Dies stellt ein Falsch Positives Ergebnis dar.

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{image/bfexample}
	\caption{BF Beispiel mit m=10 und k=3 zur Repräsentation der Menge $S=\{x_1,x_2,x_3\}$}
	\label{fig:bfexample}
\end{figure}

Die \gls{fpr} $f_r$ sollte für eine effektive Nutzung des \gls{bf} gering sein.
Um die verwendeten Parameter wie die Bit Array Größe $m$ und Anzahl der verwendeten Hashfunktionen $k$ optimal bestimmen zu können, werden deren mathematischen Formeln zur Berechnung benötigt.
Die zu speichernden Elemente können für diesen Anwendungsfall abgeschätzt werden, sodass eine mathematische Approximierung von $n$ nicht nötig ist. 

Für die Herleitung der Formeln wird angenommen, dass die Hashfunktionen jede Array Position mit gleicher Wahrscheinlichkeit auswählt. 
\begin{equation}\label{eq:fprate}
    f_r= [ 1 - ( 1 - \frac{1}{m} )^{nk} ]^k \approx (1-e^{-\frac{kn}{m}})^k 
\end{equation}
\autoref{eq:fprate} zeigt die Funktion zur Berechnung der \gls{fpr}, wobei $(1-1/m)^{nk}$ nach $e^{-kn/m}$ approximiert wird.

\begin{equation}\label{eq:k}
    k=\frac{m}{n}ln2 \approx 0.693\frac{m}{n}
\end{equation}
Um die \gls{fpr} möglichst gering zu halten, lässt sich \autoref{eq:k} für die Anzahl der Hashfunktionen aufstellen.
Durch das Einsetzen vom ermittelten $k$ in \autoref{eq:fprate}, Vereinfachen der Formel und anschließende Auflösen nach $m$ ergibt sich: 

\begin{equation}\label{eq:m}
    m=-\frac{n\ln f_r}{(\ln 2)^2} 
\end{equation}

Eine detaillierte Herleitung der einzelnen Gleichungen wird in der Arbeit von Mitzenmacher et al. durchgeführt \autocite{broder_network_2004}.

\newpage

\subsection{Merkmale}
\label{subsec:merkamle}
Nach Luo et al. sind die drei Hauptmerkmale eines \gls{bf} folgende:
\begin{itemize}
    \item Speichereffizient
    \item Konstante Geschwindigkeiten der Operationen
    \item Einseitige Fehler
\end{itemize}
Die Bit Array Größe $m$ ist proportional zu der Anzahl der Elemente $n$ \autocite{starobinski_efficient_2003}.
Die Speichereffizienz erklärt sich dadurch, dass jedes Element durch $k$ Hashfunktionen durchläuft und entsprechend $k$ viele Bits im \gls{bf} manipuliert werden.
Das Bit Array repräsentiert somit alle $n$ Elemente der Menge $A$ ohne die Größe oder Länge der einzelnen Elemente zu berücksichtigen.

Bei der Abfrage, ob sich ein Element im \gls{bf} befindet, werden lediglich $k$ Bits vom Bit Array überprüft.
Somit lässt sich bei der Abfrage ein Komplexitätsmaß von $O(k)$ feststellen.
Nachdem der Bloomfilter implementiert und $k$ definiert ist, befindet sich das Komplexitätsmaß sowohl für die Einfüge- als auch Abfrageoperation bei $O(1)$, welches eine konstante Wachstumsfunktion darstellt und somit deutlich schneller als logarithmische Funktionen ist.

Falsch Positive Ergebnisse sind bei einem \gls{bf} unvermeidbar.
Obwohl $x_5 \notin A$ gilt, kann der \gls{bf} bei einer Abfrage $x_5 \in A$ ermitteln.
Sofern Löschoperationen ausgeschlossen sind, können auch keine falsch negativen Fehler entstehen, sodass dem Urteil eines \gls{bf} vertraut werden kann, wenn dieser schlussfolgert, dass ein Element sich nicht in der Menge befindet.    
   
\subsection{Herausforderungen}
\label{subsec:herausforderungen}
Nach Luo et al. sind die größten Herausforderungen von \gls{bf} folgende:
\begin{itemize}
    \item Falsch Positiv Ergebnisse
    \item Implementierung
    \item Elastizität
    \item Funktionalität
\end{itemize}

Die \gls{fpr} kann niedrig gehalten werden, indem die genannten Parametern $m$, $k$ und $n$ optimal konfiguriert werden.
Auch wenn diese Rate niedrig eingestellt ist, können für manche Anwendungen Falsch Positive Ergebnisse inakzeptabel sein.
Damit dies möglichst selten geschieht, müssen Parameter wie die Länge des \gls{bf} und die Anzahl der Hashfunktionen erhöht werden, welches die Komplexität deutlich steigert und den verwendeten Speicher erhöht.

Bei der Implementierung müssen verschiedene Faktoren, wie der Speicherzugriff, benötigte Speichergröße und Rechenleistung berücksichtigt werden. 
Komplexe Hashfunktionen wie \gls{rsa}, \gls{sha}-1 und \gls{md}-5 sind vorallem für leichtgewichtigte Hardware und kleinere Systeme zu komplex und verbrauchen zu viel Rechenleistung. 
Bei einem größeren Datenstrom würde die Performance des \gls{bf} leiden und somit nicht mehr rentabel sein.

Nachdem die Parametern initialisiert und zugewiesen sind, können dieses nachträglich nicht mehr geändert werden. 
Somit ist ein \gls{bf} eine statische Datenstruktur.
Wenn sich die Anzahl der zu speichernden Elemente der Menge $A$ deutlich erhöht, kann dies dazu führen, dass alle Bits des \gls{bf} auf 1 stehen und die Verwendung des \gls{bf}s wäre nicht mehr sinnvoll.

Der klassische \gls{bf} verfügt über zwei Operationen.
Das Löschen eines Elements kann im \gls{bf} nicht abgebildet werden.
Wenn Elemente aus der Menge $A$ gelöscht bzw. für den \gls{bf} nicht mehr relevant werden, erhöht sich die \gls{fpr} deutlich.
Somit ist die Funktionalität des \gls{bf} lediglich auf das Hinzufügen und Abfragen eines Elements beschränkt. 

Insgesamt kann somit zusammengefasst werden, dass die Festlegung der Parameter die größte Herausforderung bei der Implementierung eines Bloomfilters ist, da verschiedene Faktoren negativ auf die \gls{fpr} einwirken können.
   