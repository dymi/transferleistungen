﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp
{
    public static class Extensions
    {
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid());
        }
    }

    internal static class Program
    {
        private static void Main(string[] args)
        {
            var configs = new List<KeyValuePair<long, float>>
            {
                new KeyValuePair<long, float>(100, 0.01f),
                new KeyValuePair<long, float>(1000, 0.01f), 
            };
            foreach (var (n, fpr) in configs)
            {
                long falsePositives = 0;
                var testdataCount = n * 100;
                for (var j = 0; j < 100; j++)
                {
                    var bloomfilter = new Ohbf(n, fpr);
                    var customers = DataGenerator.GetData(testdataCount);
                    var enumerator = customers.GetEnumerator();

                    for (var i = 0; i < n; i++)
                    {
                        enumerator.MoveNext();
                        var customer = enumerator.Current;
                        bloomfilter.Add(customer.FormatToString());
                    }

                    enumerator.Dispose();
                    var inCash = customers.Count(customer => bloomfilter.InCashe(customer.FormatToString()));
                    falsePositives = inCash - n;
                }

                var fprPrototyp = (float) falsePositives / testdataCount;
                var fprDiff = Math.Abs(fprPrototyp - fpr);
            }
        }
    }
}