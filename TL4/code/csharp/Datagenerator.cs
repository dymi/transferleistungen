using System;
using System.Collections.Generic;

namespace csharp
{
    public static class DataGenerator
    {
        public static IEnumerable<Customer> GetData(long count)
        {
            for (var i = 0; i < count; i++)
            {
                yield return new Customer(i);
            }
        }
    }

    public class Customer
    {
        public long Id { get; }

        public Customer(long id)
        {
            Id = id;
        }
    }

    public static class CustomerFormatter
    {
        public static string FormatToString(this Customer customer)
            => customer.Id.ToString();
    }
}