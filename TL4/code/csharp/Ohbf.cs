using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.HashFunction.Jenkins;
using System.Linq;
using System.Text;
using System.Threading.Channels;

namespace csharp
{
    public class Ohbf
    {
        private BitArray[] _bitArrays;

        public Ohbf(long n, float fpr)
        {
            var m = GetSize(fpr, n);
            var k = GetHashcount(m, n);
            GenerateBitArrays(m, k);
        }

        public void Add(string element)
        {
            var e = Encoding.ASCII.GetBytes(element);
            var hash = ComputeHash(e);
            foreach (var t in _bitArrays)
            {
                var h = hash % t.Count;
                t[h] = true;
            }
        }

        public bool InCashe(string element)
        {
            var e = Encoding.ASCII.GetBytes(element);
            var hash = ComputeHash(e);
            foreach (var t in _bitArrays)
            {
                var h = hash % t.Count;
                if (!t[h]) return false;
            }

            return true;
        }

        public void ShowBitArrays()
        {
            for (var i = 0; i < _bitArrays.Length; i++)
            {
                var s = "";
                for (var j = 0; j < _bitArrays[i].Length; j++)
                    s += _bitArrays[i][j] ? "1" : "0";
                Console.WriteLine($"{i + 1}. Partition: {s}");
            }
        }

        private void GenerateBitArrays(long m, int k)
        {
            var p = DeterminePartitionlenghts(m, k).OrderBy(x => x).ToArray();
            _bitArrays = new BitArray[p.Length];
            for (var i = 0; i < p.Length; i++)
                _bitArrays[i] = new BitArray(p[i]);
        }

        private static int ComputeHash(byte[] element)
        {
            var lookup3 = JenkinsLookup3Factory.Instance.Create();
            var hash = lookup3.ComputeHash(element);
            return Math.Abs(BitConverter.ToInt32(hash.Hash));
        }


        private static long GetSize(float fpr, long n)
            => Convert.ToInt64(-(n * Math.Log(fpr)) / Math.Pow(Math.Log(2), 2));

        private static int GetHashcount(long m, long n)
            => Convert.ToInt32((m / n) * Math.Log(2));

        private static IEnumerable<int> DeterminePartitionlenghts(long m, int k)
        {
            //Console.WriteLine($"m_opt = {_m}   k = {_k}");
            //Console.WriteLine($"abgerundet m_opt/k = {_m / _k}");
            var r = Convert.ToInt32(m / k);
            var pTable = GeneratePrimes2(r, k).OrderBy(x => x).ToList();
            var closestPrime = pTable.OrderBy(x => Math.Abs((long) x - r)).First();
            var idx = pTable.IndexOf(closestPrime);
            //Console.WriteLine($"p (abgerundete naeheste Primzahl): {closestPrime}");
            var sum = 0;
            var l = new List<int>();
            for (var i = idx - 1; i >= idx - k; i--)
            {
                sum += pTable[i];
                l.Add(pTable[i]);
            }

            // Console.WriteLine(
            //    $"sum (summierte k-viele Primzahlen unter p): {l.Select(x => x.ToString()).Aggregate((a, b) => a + " + " + b)} = {sum}");
            var min = Math.Abs(sum - m);
            // Console.WriteLine(
            //     "Schleifendurchlaeufe, welche den verwendeten Indexbereich der Primzahlen stetig erhöht, bis das geringste Minimum erreicht ist.");
            while (true)
            {
                //Console.WriteLine($"min: {min}, idx: {idx}");
                sum += pTable[idx] - pTable[idx - k];
                var diff = Math.Abs(sum - m);
                if (diff >= min) break;
                min = diff;
                idx += 1;
            }

            //  Console.WriteLine(
            //      "Schleifendurchlaeufe, welche k-viele Primzahlen unter dem ermittelten Indexwert als Partitionsgrößen ermitteln.");
            for (var i = 1; i <= k; i++)
                yield return pTable[idx - i];
        }

        private static IEnumerable<int> GeneratePrimes(int n)
            => Enumerable.Range(2, n - 1)
                .Where(i => Enumerable.Range(1, (int) Math.Sqrt(i))
                    .All(j => j == 1 || i % j != 0));

        // Generate Primes near n
        // Much faster than GeneratePrimes!
        // k can be added with delta value for big n
        private static IEnumerable<int> GeneratePrimes2(int n, int k)
        {
            int x = k;
            for (int i = n; i > 1; i--)
            {
                if (IsPrime(i))
                {
                    yield return i;
                    x--;
                }
                if (x < 0)
                    break;
            }
            x = k;
            while (x >= 0)
            {
                if (IsPrime(n))
                {
                    yield return n;
                    x--;
                }
                n++;
            }
        }

        private static bool IsPrime(int number)
        {
            if (number == 1) return false;
            if (number == 2) return true;

            var limit = Math.Ceiling(Math.Sqrt(number)); //hoisting the loop limit

            for (int i = 2; i <= limit; ++i)
                if (number % i == 0)
                    return false;
            return true;
        }

        public string GetParaInfos()
        {
            var m = _bitArrays.Sum(x => x.Count);
            return $"m = {m} | Speicherverbrauch = {_bitArrays.Sum(x => x.Count) / (8f * 1000000)} MBytes";
        }
    }
}