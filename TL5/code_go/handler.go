package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

var counter = 0

func main() {
	listenAddr := ":8080"
	if val, ok := os.LookupEnv("FUNCTIONS_CUSTOMHANDLER_PORT"); ok {
		listenAddr = ":" + val
	}
	http.HandleFunc("/api/GoTEST", countHandler)
	log.Fatal(http.ListenAndServe(listenAddr, nil))
}

func countHandler(w http.ResponseWriter, r *http.Request) {
	counter++
	if counter == 1 {
		log.Print("Cold Start detected.")
	} else {
		log.Print(counter)
	}

	fmt.Fprint(w, counter)
}
