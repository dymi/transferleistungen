use std::env;
use std::net::Ipv4Addr;
use warp::{http::Response, Filter};

static mut COUNTER: i32 = 0;

#[tokio::main]
async fn main() {
    let counthandler = warp::get()
        .and(warp::path("api"))
        .and(warp::path("RustTEST"))
        .map(unsafe {
            || {
                COUNTER += 1;
                if COUNTER == 1 {
                    println!("Cold Start detected.")
                } else {
                    println!("{}", COUNTER)
                }
                Response::builder().body(format!("{}", COUNTER))
            }
        });

    let port_key = "FUNCTIONS_CUSTOMHANDLER_PORT";
    let port: u16 = match env::var(port_key) {
        Ok(val) => val.parse().expect("Custom Handler port is not a number!"),
        Err(_) => 3000,
    };

    warp::serve(counthandler)
        .run((Ipv4Addr::UNSPECIFIED, port))
        .await
}
