using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace My.Functions {
    public static class CSharpTest {

        private static int _counter = 0;

        [FunctionName ("CSharpTEST")]
        public static async Task<IActionResult> Run (
            [HttpTrigger (AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log) {
            var c = IncreaseCounter ();
            if (c == 1)
                log.LogInformation ("Cold Start detected.");
            else
                log.LogInformation ($"{c}");
            return new OkObjectResult (c);
        }

        private static int IncreaseCounter () {
            _counter++;
            return _counter;
        }
    }
}