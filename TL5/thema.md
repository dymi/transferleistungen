# Cold Start Problem
* Serverless Function is hot when running
* Provider will drop the container after a period of inactivity
* Cold Start happens when execute a cold function
* Its a DELAY Problem, cause the cloud provider need to start up the container 
* This process will considerably increase your execution time

## Fragen:
* Welche Prozesse sind betroffen?
* Workflow der Funktion; für Fachlichkeit
* Wie heißen die Funktionen, was tun sie genau; Bilder Zugriff bitte..
* Welche Ansätze zur Lösung wurden bereits unternommen?
* Was sind bekannte Ansätze?
* Was kann dadurch eingespart werden? Wichtigkeit der Arbeit

Alle Kommunikation zwischen Domainen werden in Azure über Events und Functions ausgelöst
200 Functions; zu teuer um dauerhauft laufen zu lassen. 
Consumtion-Model ist ein Bezahlmodel, bezahl nur das was du verbrauchst

Events starten morgens, kommen von außen, Makrtkommunkationgeschichten, Umzug etc. 
Function ist die Reaktion die getriggert werden, laufen auf Timeouts
Event landet in DEADLETTERQUEUE und ist weg! 
Bis zu einer Stunde bis alles wach ist!
Restarts erfordernlich

Sleep Kaskaden! Cold Start Kaskaden.
Nachts passiert nichts; am morgen gehen die ersten Events verloren. 
Retry Machanismus ist schwieirig, da keiner Ahnung wie oft es knallen kann
Cache aus Function wird teils verwendet.

Typisches C# Problem, schlechtes Codestartverhalten:
Just In Time Kompiler wird geladen, lädt Dotnet Runtim, Kompiliert Programm
Serialisierung/Deserialisierung, DI pro Objekt drei mal die Kette durchlaufen. 
Daher ist C# nicht ganz geeignet pausiert zu werden. 
Oder andere Möglichkeiten, VOrkompilierung etc.

Cold Start Behaviert Vergleich zwischen Go, Rust und C#.

Einige Functions über Service-Model umgestellt, Bezahlmodel
Identifizieren der Functions welche kritisch sind

1.200€ Pro Monat für eine Function
200€ Pro Monat für einfache Functions
Arbeitsspeicher wird verschwendet, C# verbraucht da viel, das sind hohe Kosten.

# Links:
* [Quickstart: Create a C# Function in Azure from the command line](https://docs.microsoft.com/en-us/azure/azure-functions/create-first-function-cli-csharp?tabs=azure-cli%2Cin-process)
* 