\chapter{Fazit und Ausblick}
\label{chap:fazitundausblick}
In diesem Kapitel wird ein Fazit zur Arbeit und einen entsprechenden Ausblick gegeben.

\section{Fazit}
\label{sec:fazit}
Es wurden \textit{Cloud Functions} in Go, Rust und C\# implementiert und die Dauer von Kaltstarts der jeweiligen Umgebungen gemessen. 
Diese Daten wurden gegenübergestellt und kritisch bewertet.
Es hat sich gezeigt, dass die Implementierung von \textit{Custom Handlers} mit einen zusätzlichen Start eines Webservers betroffen sind.
Durch diesen Start und die Weiterleitung der \gls{http}-Aufrufe an diesen Server erhöht sich der Kaltstart der Anwendungen um das doppelte im Vergleich von herkömmlichen C\# Funktionen.

Durch die systematische Beantwortung der Forschungsfragen F1 und F2 konnte in \autoref{chap:diskussion} die letzte Forschungsfrage somit beantwortet werden, dass ohne die Einbindung eines Webservers, der Kaltstart von Go und Rust Funktionen gegenüber von C\# Funktionen schneller sein müsste, da diese Programme bereits in nativen Code kompiliert sind. 
Jackson et al. haben mit ihrer Arbeit diese Annahme bestätigt.

Wenn Azure die Implementierung der \textit{Custom Handlers} weiter optimiert, sodass kein Webserver als Proxy für spezielle Laufzeitumgebungen benötigt wird, ist der Austausch von C\# Funktionen in Go oder Rust Funktionen weiterhin eine mögliche Lösung für die Kaltstartproblematik. 

\section{Ausblick}
\label{sec:ausblick}
Der Austausch von C\# in Go/Rust Funktionen ist aufgrund der zusätzlichen Einbindung von Webservers keine mögliche Lösung für die Kaltstartproblematik von C\# \textit{functions}.
Es gilt durch weitere Experimente und Analysen festzustellen, wie man die Kaltstartproblematik von C\# Funktionen verbessern kann.

Da in diesem Versuch lediglich einfache Functions mit wenig Abhängigkeiten implementiert wurden, kann ein weiterer Versuch mit realitätsnahen Functions, welche eine wesentlich höhere Abhängigkeiten zu weiteren Bibliotheken haben, implementieren werden.
Die Vermutung hierbei ist, dass die Kompilierung des \gls{jit} Compilers der .NET Dateien, länger benötigt als die direkte Ausführung des nativen Codes von Go oder Rust Programmen.
Wenn die Kompilierung des C\# Programms mehr Zeit benötigt als der Start des Webservers für die \textit{Custom Handlers}, dann wäre somit der Kaltstart von Go und Rust Funktionen effizienter und der Austausch der C\# zu Go/Rust Funktionen weiterhin eine mögliche Lösung.

Außerdem bietet Azure auch Laufzeitumgebungen für \textit{Cloud Functions} an, wie JavaScript und Python. 
Die Arbeiten von Manner et al. und Varshney et al. bestätigen, dass interpretierbare Sprachen deutlich schneller einen Kaltstart ausführen als kompilierbare Sprachen. 
Die Ergebnisse von Jackson et al. liefern für Python sogar die besten Durchschnittswerte für die Ausführung eines Kaltstarts.
Somit könnte die Evaluierung der Kaltstartproblematik von JavaScript und Python gegenüber C\# Funktionen eine weitere Lösung sein.

Um einen Kaltstart entgegen zu wirken, besteht auch die Lösung darin, die Funktionen vor Verwendung mittels eines Pingrequests aufzurufen und somit auf einen Warmstart vorzubereiten.
Hierfür müssen diverse Informationen ermittelt werden, unter anderem welche Funktionen betroffen sind und zur welcher Zeit diese wahrscheinlich aufgerufen werden.
Durch die Anzahl und Abhängigkeiten der \textit{events} und \textit{functions} untereinander, ist die Ermittlung und Analysen dieser Daten eine  komplexe Aufgabe.  
