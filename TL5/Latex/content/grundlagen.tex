\chapter{Grundlagen zur Quellcodekompilierung}
\label{chap:grundlagen}
Um die erste Forschungsfrage F1 beantworten zu können, werden in diesem Kapitel Grundlagen zur Kompilierung von Quellcode geschaffen. 
Dabei werden die jeweiligen Kompilierungsprozesse von Go, Rust und C\# gegenübergestellt. 

\section{Kompilierungsprozess in C\#}
\label{sec:kompilierungsprozesscsharp}
C\# ist eine von Microsoft entwickelte universelle Multi-Paradigma Programmiersprache und gehört zum .NET Framework. 
Das .NET Framework war die vorherrschende Implementierung der \gls{cli}, bis sie durch das plattformübergreifende .NET-Projekt ersetzt wurde.
Die \gls{cli} wird in ECMA-335 als internationale Norm definiert \autocite{noauthor_ecma-335_nodate}.
Sie beschreibt die Infrastruktur des .NET Ökosystems und gibt \gls{cli} Sprachen eine Basis, um ihr Typsystem zu bilden. 
Außerdem können Anwendungen, welche in mehreren Hochsprachen geschrieben sind, in verschiedenen Systemumgebungen ausgeführt werden, ohne dass diese umgeschrieben werden müssen.
Die \gls{cli} Spezifikation definiert unter anderem auch die \gls{cil} bzw. \gls{il}, welche einen binären und stapelbasierten Befehlssatz darstellt.
Alle Sprachen, welche auf die \gls{cli} abzielen, werden zu \gls{il} kompiliert.
\gls{il} Anweisungen werden von einer \gls{cli} kompatiblen Laufzeitumgebung wie der \gls{clr} ausgeführt.
Diese virtuelle Anwendungsmaschine stellt verschiedene Dienste wie Sicherheit, Speicherverwaltung und Ausnahmebehandlungen bereit.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{image/ccompilation}
	\caption{C\# Kompilierungsprozess}
	\label{fig:ccompilation}
\end{figure}

\autoref{fig:ccompilation} zeigt den geschilderten C\# Kompilierungsprozess. 
Dabei ist zu erkennen, dass der Prozess in statischer und \gls{jit} Kompilierung unterteilt ist.
Die statische Kompilierung stellt sicher, dass der Code bis kurz vor der Ausführung bereits in einer \gls{il} vorkompiliert ist.
Zur Laufzeit kompiliert der \gls{jit} Compiler die tatsächlich verwendeten \gls{il} Dateien in nativen Code. \autocite{gewarren_microsoft_nodate}

\section{Kompilierungsprozess in Go}
\label{sec:kompilierungsprozessgo}
Go ist eine statisch typisierte und kompilierbare Sprache mit dem Fokus auf Effizienz, Geschwindigkeit, Skalierbarkeit und Multicore-Computing.
Es wird ein Compiler benötigt, welcher den Programmcode direkt in Maschinencode transformiert und somit ohne eigener Laufzeit auf dem Betriebssystem direkt ausgeführt werden kann. \autocite{noauthor_go_nodate}

Der Kompilierungsprozess in Go kann in vier Phasen unterteilt werden:
\begin{itemize}
	\item Grammatikalische Analyse mit \gls{ast} Umwandlung 
    \item Typüberprüfung
    \item Codeoptimierung mit \gls{ssa} Feature
    \item Maschinencode Generierung
\end{itemize}

In der ersten Phase werden die Zeichenfolgen aller Go Programmdateien in eine Sequenz aus Tokens umgewandelt, um den weiteren Prozess zu vereinfachen.
Anschließend werden die Tokens lexikalisch geordnet und mittels eines Parsers nach der Grammatik überprüft.
Das Ergebnis ist für jede Go Datei ein eigener \gls{ast}.
\gls{ast} repräsentieren die Grammatik einer Programmiersprache in einer Baumstruktur. 
Dabei stellt jeder Knoten im Baum ein Element aus dem Programmcode dar. 
\autoref{fig:ast} zeigt ein einfaches Beispiel einer \gls{ast} Struktur für den Ausdruck $8*5+2$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.25\linewidth]{image/ast}
	\caption{AST Beispiel}
	\label{fig:ast}
\end{figure}

Darauffolgend werden alle Bäume durchlaufen und die Knoten nach Typfehlern überprüft.
Nach diesen Prozessschritten kann davon ausgegangen werden, dass der Code fehlerfrei kompiliert.
Somit wird in der dritten Phase die Baumstruktur in optimierten Zwischencode transformiert.
Hierbei wird das \gls{ssa} Feature verwendet.
Dies vermeidet mehrfache Zuweisungen von Variablen.
\gls{ssa} beinhaltet außerdem die Eliminierung von nicht verwendeten Code und verhindert zusätzlich unnötige Redundanzen.
In der letzten Phase wird aus dem Zwischencode direkt Maschinencode für den ausgewählten Prozessor generiert. \autocite{noauthor_go_2022}

\section{Kompilierungsprozess in Rust}
\label{sec:kompilierungsprozessrust}
Rust ist eine Multi-Paradigma Programmiersprache, deren Hauptziele die Speichersicherheit und Performance vor allem in nebenläufigen Prozessen sind \autocite{noauthor_rust_nodate}.
Die Kompilierungsschritte sind hierbei ähnliche wie in Go.
Die beiden Hauptunterschiede liegen darin, dass die \gls{ast} nach der grammatikalischen Überprüfung in \gls{hir} Formate umgewandelt werden, welche für die Typüberprüfung schneller bzw. kompilier freundlicher seien soll.
Für die anschließende Codeoptimierung werden die \gls{hir} in \gls{mir} Dateien umgewandelt, welche insbesondere für den \textit{borrow check} nötig ist.
Der \textit{borrow check} ist die Überprüfung des Compilers, dass das \textit{Ownership}-Mechanismus funktioniert.

Beim \textit{Ownership}-Mechanismus finden hauptsächlich drei Regeln Anwendung \autocite{klabnik_rust_nodate}:
\begin{itemize}
	\item Jeder Wert in Rust hat eine Variable, die als \textit{owner} bezeichnet wird.
 	\item Es kann zu jedem Zeitpunkt nur einen \textit{owner} geben.
  	\item Wenn der \textit{owner} den Kontext verlässt, wird der Wert verworfen.
\end{itemize}

Die Verwendung des \textit{Ownership}-Mechanismus macht den Gebrauch eines \gls{gc}, eine Form der automatisierten Speicherverwaltung, in Rust obsolet.

\section{Zusammenfassung}
\label{sec:zusammenfassung}
Die betrachteten Programmiersprachen sind alle kompilierbar.
Der Hauptunterschied zwischen C\# und Go/Rust liegt darin, dass C\# eine Laufzeitumgebung benötigt, um die vorkompilierten \gls{il} Dateien auszuführen.
Go und Rust kompilieren hingegen direkten Maschinencode, welcher vom Prozessor ohne sprachabhängige Runtime ausgeführt werden kann.
