\chapter{Experiment}
\label{chap:experiment}
Um die Forschungsfrage F2 beantworten zu können, wird in diesem Kapitel das Experiment vorgestellt.
Es wurden verschiedene Prototypen in Go, Rust und C\# implementiert und als Funktionen nach Azure hochgeladen.
Anschließend wurden Kaltstarts ausgeführt, die Dauer gemessen und die Daten gegenübergestellt.

\section{Aufbau}
\label{sec:aufbau}
Um den Kaltstart von unterschiedlichen Laufzeiten zu testen, wurden in Azure drei \textit{Functionapps} bereitgestellt, welche als Hosts für \textit{functions} dienen.
Als Betriebssystem wurde jeweils eine Linux Maschine ausgewählt.
Es wurde ein Algorithmus in Go, Rust und C\# geschrieben, der dem Aufrufer mitteilt, ob der aktuelle Request ein Kaltstart der Funktion ausgelöst hat.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{image/funcflow2}
	\caption{Aufrufreihenfolge des Experiments}
	\label{fig:funcflow}
\end{figure}

Dazu wurde eine statische Variable mit der Zahl null instanziiert und beim Aufruf um eins inkrementiert. 
Wenn diese Variable anschließend gleich eins ist, dann handelt es sich bei diesem Aufruf um einen Kaltstart.
Weitere Aufrufe geben lediglich den aktuellen Wert dieser Variablen aus.
Wenn die Maschine, worin diese Funktion läuft, heruntergefahren wird, wird auch der Wert der statischen Variable gelöscht.
Somit werden die benötigten Logeinträge, womit der Kaltstart ermittelt wird, einfacher gefunden.
Die dazugehörigen Codeauszüge können im Anhang A nachgelesen werden.

\autoref{fig:funcflow} zeigt die Aufrufreihenfolge des Experiments. 
Die Funktionen werden über eine \gls{url} mittels eines \gls{http} Requests direkt angesprochen. 
Dieses \gls{http}-Gateway überprüft, ob der angegebene Pfad korrekt ist und ruft entsprechend die zugehörige \textit{Azure Function} auf.
Das Ergebnis wird über das \gls{http}-Gateway an den Aufrufer zurückgesendet.
Diese Vorgänge werden in Logdateien mittels Application Insights gespeichert.
Über Application Insights lassen sich unteranderem auch die Antwortzeiten zwischen einzelne HTTP Requests ermitteln. 
Da diese Zeiten in der Azure Umgebung gemessen werden, sind die Netzwerksauswirkungen vom Client zum Cloud Server nicht relevant für dieses Experiment.  

\section{Durchführung}
\label{sec:durchfuehrung}
Nach Durchführung lokaler Tests, ob die statische Variable sich bei einem Neustart zurücksetzt und ein Kaltstart korrekt detektiert wird, wurden die jeweiligen Programmcodes für Linux Umgebungen kompiliert und nach Azure in ihre jeweilige \textit{Functionapp} publiziert.\\

\begin{lstlisting}[caption={Codeauszug eines Scripts zur Aufrufung der Cloud Funktionen},label={lst:script},captionpos=b,style=csharp, xleftmargin=0.05\textwidth,xrightmargin=0.05\textwidth, firstnumber=1][H]
    urls = [url_csharp, url_rust, url_go]*5

    counter = 0 
    while counter < 72:
        for url in urls:
            requests.get(url)
        counter += 1
        if counter < 72:
            time.sleep(60*20)
\end{lstlisting}

\autoref{lst:script} zeigt ein Python Script, welches innerhalb von 24 Stunden alle 20 Minuten den jeweiligen Cloud Funktionen fünf \gls{http}-Anfragen stellt.
Da die Container, worin die Funktionen laufen, auf Azure so eingestellt sind, dass diese sich alle 15 Minuten nach Inaktivität abschalten, wurde mit der Ausführung des Scripts für jede Laufzeitumgebung 72 Kaltstarts und 288 Warmstarts provoziert.

Anschließend wurden die Logs aus Application Insights nach Kaltstart Ergebnissen gefiltert.
Die gemessenen Antwortzeiten aller Kaltstarts wurden tabellarisch gesammelt und die Durchschnittszeiten ausgewertet. 

\section{Beobachtung}
\label{sec:beobachtung}
Wie \autoref{tab:avgergebnis} zeigt, liegt beim Kaltstart die durchschnittliche Antwortzeit einer C\# Funktion bei $214,48 ms$ während diese bei Go und Rust mit $539,1 ms$ und $521,3 ms$ mehr als doppelt so hoch ist.
Außerdem wurden zum Vergleich auch die Warmstart Zeiten gemessen.
Diese liegen im Durchschnitt für C\# Funktionen bei $5,34 ms$.
Go und Rust Funktionen benötigen auch hier knapp doppelt so lange, um auf einen Request zu reagieren.

\begin{table}[H]
    \setlength{\arrayrulewidth}{0.5mm}
    \setlength{\tabcolsep}{18pt}
    \renewcommand{\arraystretch}{1.5}
    \centering
    \begin{tabular}{p{2cm}|p{2cm}|p{2cm}|p{2cm}}
    \hline
        & Go & Rust & C\# \\ \hline
        Kaltstart & 539,1 & 521,3 & 214,48 \\ 
        Warmstart & 9,83 & 10,53 & 5,34 \\ \hline 
    \end{tabular}
    \caption{Ergebnisse der gemessenen durchschnittlichen Antwortzeiten von Kalt- und Warmstarts in Millisekunden}
    \label{tab:avgergebnis}
\end{table}

\autoref{fig:logc} zeigt die Logs eines Requests, welcher einen Kaltstart detektiert hat.
Hier ist zu erkennen, dass die Zeit zwischen dem eingehenden Request und dem Log, dass die Funktion aufgerufen wird, $220$ ms vergangen sind. 
In dieser Zeit wurde ein Container hochgefahren, welcher entsprechend die C\# Funktion geladen hat.
Die tatsächliche Ausführung der Funktion lag in Azure bei $7$ ms.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{image/logc}
	\caption{Logs eines Kaltstart Requests}
	\label{fig:logc}
\end{figure}

Da die Beobachtungen nicht den Erwartungen entsprachen, wurden zusätzlich auf einer lokalen Entwicklungsmaschine jeweils 20 Kaltstarts durchgeführt und die Antwortzeiten gemessen.
Die durchschnittlichen Antwortzeiten lagen hierbei für Go und Rust bei $80$ ms, während C\# mit $48,6$ ms auch hier deutlich schneller ist.

Durch die gesammelten Informationen und die beobachteten Werte, kann die Forschungsfrage F2 nicht bestätigt werden. 
Da die Kompilierung von Quellcode in Go und Rust prinzipiell schneller ist als von C\#, müssen weitere Faktoren Einfluss auf den Kaltstart von \textit{Azure Functions} haben.  

