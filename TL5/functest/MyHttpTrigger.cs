using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace functest {
    public static class MyHttpTrigger {
        [FunctionName ("Fib")]
        public static async Task<IActionResult> Run (
            [HttpTrigger (AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log) {
            log.LogInformation ("C# HTTP trigger function processed a request.");
            string n = req.Query["n"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            n = n ?? data?.n;

            var fib = Fib(Int32.Parse(n));

            return new OkObjectResult (fib);
        }

        private static int Fib (int n) {
            if (n == 0)
                return 0;
            if (n == 1) 
                return 1;
            else 
                return (Fib(n-1)+Fib(n-2));
        }
    }
}